﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210423_CoffeeDispenserDecorator_DLL
{
    public class Coffee : ABeverage
    {
        public Coffee() : base("Kaffee", 0.50) { }
    }
    public class Tea : ABeverage
    {
        public Tea() : base("Tee", 0.40) { }
    }
    public class HotChocolate : ABeverage
    {
        public HotChocolate() : base("HotChocolate", 0.60) { }
    }
  
    public class WhiteChocolate : ABeverage
    {
        public WhiteChocolate() : base("WhiteChocolate", 1.0) { }
    }
    public class CaramelLate : ABeverage
    {
        public CaramelLate() : base("CaramelLate", 1.0) { }
    }
}
