﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210423_CoffeeDispenserDecorator_DLL
{
    public class CoffeeDispenserOutOfFuelException : Exception
    {
        public CoffeeDispenserOutOfFuelException(string message) : base(message) { }
    }
}
