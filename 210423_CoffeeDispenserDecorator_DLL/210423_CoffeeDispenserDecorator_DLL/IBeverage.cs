﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210423_CoffeeDispenserDecorator_DLL
{
    public interface IBeverage
    {
        string GetDescription();
        double GetPrice();
    }
}
