﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210423_CoffeeDispenserDecorator_DLL
{
    class Milk : AIngredientDecorator
    {
        public Milk(IBeverage beverage) : base("Milch", 0.10, beverage) { }
    }
    class SojaMilk : AIngredientDecorator
    {
        public SojaMilk(IBeverage beverage) : base("Sojamilch", 0.20, beverage) { }
    }
    class OatMilk : AIngredientDecorator
    {
        public OatMilk(IBeverage beverage) : base("Hafermilch", 0.20, beverage) { }
    }
    class LactoseFreeMilk : AIngredientDecorator
    {
        public LactoseFreeMilk(IBeverage beverage) : base("Laktosefreie Milch", 0.20, beverage) { }
    }
    class Sugar : AIngredientDecorator
    {
        public Sugar(IBeverage beverage) : base("Zucker", 0.10, beverage) { }
    }
    class BrownSugar : AIngredientDecorator
    {
        public BrownSugar(IBeverage beverage) : base("Brauner Zucker", 0.20, beverage) { }
    }
    class Xylit : AIngredientDecorator
    {
        public Xylit(IBeverage beverage) : base("Xylit", 0.20, beverage) { }
    }
    class Stevia : AIngredientDecorator
    {
        public Stevia(IBeverage beverage) : base("Stevia", 0.20, beverage) { }
    }
}
