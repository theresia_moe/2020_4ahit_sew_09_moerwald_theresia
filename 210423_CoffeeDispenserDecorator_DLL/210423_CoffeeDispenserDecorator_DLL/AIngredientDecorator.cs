﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210423_CoffeeDispenserDecorator_DLL
{
    class AIngredientDecorator : ABeverage
    {
        protected IBeverage beverage;
        protected AIngredientDecorator(
            string description, double price,
            IBeverage beverage) : base(description, price)
        {
            this.beverage = beverage;
        }
        public override string GetDescription()
        {
            return $"{beverage.GetDescription()}" + $"{this.description}";
        }
        public override double GetPrice()
        {
            return beverage.GetPrice() + price;
        }
        public void SetBeverage(IBeverage beverage)
        {
            this.beverage = beverage;
        }
        public override string ToString()
        {
            return $"{GetDescription()}" + $"kostet {GetPrice()}";
        }
    }
}
