﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210423_CoffeeDispenserDecorator_DLL
{
    public class CoffeeDispenser
    {
        private int amountOfCoffee = 0; 
        private int amountOfTea = 0;
        private int amountOfHotChocolate = 0;       
        private int amountOfWhiteChocolate = 0;
        private int amountOfCaramelLate = 0;
        private int amountOfMilk = 0;
        private int amountOfSojaMilk = 0;
        private int amountOfOatMilk = 0;
        private int amountOfLactosefreeMilk = 0;
        private int amountOfSugar = 0;
        private int amountOBrownSugar = 0;
        private int amountOfXylit = 0;
        private int amountOfStevia = 0;
        private double curAccountBalance = 0;
        public double CurAccountBalance { get { return curAccountBalance; } set { this.curAccountBalance = value; } }

        public void Refill(
            int amountOfTea,
            int amountOfHotChocolate,
            int amountOfCoffee,
            int amountOfFruitTea,
            int amountOfKakaoWhiteChocolate,
            int amountOfMilk,
            int amountOfSojaMilk,
            int amountOfOatMilk,
            int amountOfUhtMilk,
            int amountOfSugar,
            int amountOfCaneSugar,
            int amountOfBirchSugar,
            int amountOfCoconutblossomSugar)
        {
            this.amountOfTea += amountOfTea;
            this.amountOfHotChocolate += amountOfHotChocolate;
            this.amountOfCoffee += amountOfCoffee;
            this.amountOfWhiteChocolate += amountOfFruitTea;
            this.amountOfCaramelLate += amountOfKakaoWhiteChocolate;
            this.amountOfMilk += amountOfMilk;
            this.amountOfSojaMilk += amountOfSojaMilk;
            this.amountOfOatMilk += amountOfOatMilk;
            this.amountOfLactosefreeMilk += amountOfUhtMilk;
            this.amountOfSugar += amountOfSugar;
            this.amountOBrownSugar += amountOfCaneSugar;
            this.amountOfXylit += amountOfBirchSugar;
            this.amountOfStevia += amountOfCoconutblossomSugar;
        }
        private void DecreaseTea()
        {
            if (amountOfTea == 0)
            {
                throw new CoffeeDispenserOutOfFuelException("CoffeeDispenser out of Tea");
            }
            amountOfTea--;
        }
        private void DecreaseHotChocolate()
        {
            if (amountOfHotChocolate == 0)
            {
                throw new CoffeeDispenserOutOfFuelException("CoffeeDispenser out of Kakako");
            }
            amountOfHotChocolate--;
        }
        private void DecreaseCoffee()
        {
            if (amountOfCoffee == 0)
            {
                throw new CoffeeDispenserOutOfFuelException("CoffeeDispenser out of Coffee");
            }
            amountOfCoffee--;
        }
        private void DecreaseWhiteChocolate()
        {
            if (amountOfWhiteChocolate == 0)
            {
                throw new CoffeeDispenserOutOfFuelException("CoffeeDispenser out of FruitTea");
            }
            amountOfWhiteChocolate--;
        }
        private void DecreaseCaramelLate()
        {
            if (amountOfCaramelLate == 0)
            {
                throw new CoffeeDispenserOutOfFuelException("CoffeeDispenser out of KakaoWhiteChocolate");
            }
            amountOfCaramelLate--;
        }
        private void DecreaseMilk()
        {
            if (amountOfMilk == 0)
            {
                throw new CoffeeDispenserOutOfFuelException("CoffeeDispenser out of Milk");
            }
            amountOfMilk--;
        }
        private void DecreaseSojaMilk()
        {
            if (amountOfSojaMilk == 0)
            {
                throw new CoffeeDispenserOutOfFuelException("CoffeeDispenser out of SojaMilk");
            }
            amountOfSojaMilk--;
        }
        private void DecreaseOatMilk()
        {
            if (amountOfOatMilk == 0)
            {
                throw new CoffeeDispenserOutOfFuelException("CoffeeDispenser out of OatMilk");
            }
            amountOfOatMilk--;
        }
        private void DecreaseLactoseFreeMilk()
        {
            if (amountOfLactosefreeMilk == 0)
            {
                throw new CoffeeDispenserOutOfFuelException("CoffeeDispenser out of UhtMilk");
            }
            amountOfLactosefreeMilk--;
        }
        private void DecreaseSugar()
        {
            if (amountOfSugar == 0)
            {
                throw new CoffeeDispenserOutOfFuelException("CoffeeDispenser out of Sugar");
            }
            amountOfSugar--;
        }
        private void DecreaseBrownSugar()
        {
            if (amountOBrownSugar == 0)
            {
                throw new CoffeeDispenserOutOfFuelException("CoffeeDispenser out of CaneSugar");
            }
            amountOBrownSugar--;
        }
        private void DecreaseXylit()
        {
            if (amountOfXylit == 0)
            {
                throw new CoffeeDispenserOutOfFuelException("CoffeeDispenser out of BirchSugar");
            }
            amountOfXylit--;
        }
        private void DecreaseStevia()
        {
            if (amountOfStevia == 0)
            {
                throw new CoffeeDispenserOutOfFuelException("CoffeeDispenser out of CoconutBlossomSugar");
            }
            amountOfStevia--;
        }
        public IBeverage AddMilk(IBeverage b)
        {
            DecreaseMilk();
            return new Milk(b);
        }
        public IBeverage AddSojaMilk(IBeverage b)
        {
            DecreaseSojaMilk();
            return new SojaMilk(b);
        }
        public IBeverage AddOatMilk(IBeverage b)
        {
            DecreaseOatMilk();
            return new OatMilk(b);
        }
        public IBeverage AddLactosefreeMilk(IBeverage b)
        {
            DecreaseLactoseFreeMilk();
            return new LactoseFreeMilk(b);
        }
        public IBeverage AddSugar(IBeverage b)
        {
            DecreaseSugar();
            return new Sugar(b);
        }
        public IBeverage AddBrownSugar(IBeverage b)
        {
            DecreaseBrownSugar();
            return new BrownSugar(b);
        }
        public IBeverage AddXylit(IBeverage b)
        {
            DecreaseXylit();
            return new Xylit(b);
        }
        public IBeverage AddStevia(IBeverage b)
        {
            DecreaseStevia();
            return new Stevia(b);
        }
        public IBeverage GetCoffee()
        {
            DecreaseCoffee();
            return new Coffee();
        }
        public IBeverage GetTea()
        {
            DecreaseTea();
            return new Tea();
        }
        public IBeverage GetHotChocolate()
        {
            DecreaseHotChocolate();
            return new HotChocolate();
        }
        public IBeverage GetWhiteChocolate()
        {
            DecreaseWhiteChocolate();
            return new WhiteChocolate();
        }
        public IBeverage GetCaramelLate()
        {
            DecreaseCaramelLate();
            return new CaramelLate();
        }
    }
}
