﻿using System;

namespace _210423_CoffeeDispenserDecorator_DLL
{
    abstract public class ABeverage : IBeverage
    {
        protected string description;
        protected double price;

        protected ABeverage(string description, double price)
        {
            this.description = description;
            this.price = price;
        }
        public virtual string GetDescription()
        {
            return description;
        }
        public virtual double GetPrice()
        {
            return price;
        }
        public override string ToString()
        {
            return $"{description} kostet {price}";
        }
    }
}
