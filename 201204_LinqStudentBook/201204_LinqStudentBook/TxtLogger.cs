﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201204_LinqStudentBook
{
    public class TxtLogger : ILogger
    {
        public TxtLogger(string path)
        {
            this.path = path;
        }
        public string path { get; set; }

        public void Log(string message, string result)
        {
            using (StreamWriter ws = new StreamWriter(path, true))
            {
                ws.WriteLine(message + " => " + result);
            }
        }
    }
}
