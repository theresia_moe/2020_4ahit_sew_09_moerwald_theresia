﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static _201204_LinqStudentBook.StudentBook;

namespace _201204_LinqStudentBook
{
    public class LinqMethods
    {

        // Average
        public static double AverageStudentAge(List<Student> students)
        {
            return students.Average(s => s.Age);
        }

        //Sum
        public static int SumBookPages(List<Book> books)
        {
            return books.Sum(s => s.Pages);
        }

        //Count
        public static int CountStudents(List<Student> students)
        {
            return students.Count();
        }

        //Min
        public static List<Book> CheapestBook(List<Book> books)
        {
            return books.Where(s => s.Price == books.Min(b => b.Price)).ToList();
        }
        //Max
        public static List<Book> MostExpensiveBook(List<Book> books)
        {
            return books.Where(s => s.Price == books.Max(b => b.Price)).ToList();
        }

        //Orderby
        public static List<Book> OrderByBookPages(List<Book> books)
        {
            return books.OrderBy(s => s.Pages).ToList();

        }

        //Thenby, ToDictionary
        public static Dictionary<int, string> OrderByNameThenID(List<School> schools)
        {
            return schools.OrderBy(s => s.Name).ThenBy(s => s.Id).ToDictionary(s => s.Id, s=> s.Name);
                       
        }

        //OfType, ToArray
        public static School[] OfTypeSchool(List<object> schools)
        {
            return schools.OfType<School>().ToArray();
        }

        //Descending, Skip
        public static List<Book> OrderbyPagesDescendingSkip(List<Book> books, int skipcount)
        {

            return books.OrderByDescending(s => s.Pages).Skip(skipcount).ToList();
        }


        //Reverse
        public static List<Student> ReverseStudentNames(List<Student> students)
        {
            students.Reverse();
            return students;
        }

        //Take
        public static List<School> TakeNumberOfSchool(List<School> schools, int takecount)
        {
            return schools.Take(takecount).ToList();
        }

        //Concat
        public static List<School> ConcatSchools(List<School> school, List<School> school2)
        {
            return school.Concat(school2).ToList();
        }

        //Distinct
        public static List<School> RemoveRedundantSchools(List<School> school)
        {
            return school.Distinct().ToList();
        }

        //Expect
        public static List<School> ExpectSchools(List<School> school, List<School> school2)
        {
            return school.Except(school2).ToList();
        }

        //Intersect
        public static List<School> IntersectSchools(List<School> school, List<School> school2)
        {
            return school.Intersect(school2).ToList();
        }

        //Union
        public static List<School> UnionSchools(List<School> school, List<School> school2)
        {
            return school.Union(school2).ToList();
        }

    }

}
