﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace _201204_LinqStudentBook
{
    public class XMLLogger : ILogger
    {
        public XMLLogger(string path)
        {
            this.path = path;
            if (!File.Exists(path))
                xDocument = XDocument.Parse(@"<Log></Log>");
            else
                xDocument = XDocument.Load(path);
        }
        XDocument xDocument = new XDocument();
        public string path { get; set; }
        public void Log(string testmethod, string message)
        {
            XElement testLog = new XElement("TestLog", new XAttribute("method", testmethod),
                new XElement("message", message));

            xDocument.Element("Log").Add(testLog);
            xDocument.Save(path);
        }
    }
}
