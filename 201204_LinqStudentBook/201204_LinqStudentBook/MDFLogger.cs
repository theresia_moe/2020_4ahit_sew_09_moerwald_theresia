﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201204_LinqStudentBook
{
    public class MDFLogger : ILogger
    {

        public MDFLogger(string Path = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Theresia\Documents\Schule\SEW\2020_4ahit_sew_09_moerwald_theresia\201204_LinqStudentBook\LogTest.mdf;Integrated Security=True;Connect Timeout=30")
        {
            connection = new SqlConnection(Path);
            connection.Open();
        }
        public string path { get; set; }
        SqlConnection connection;
        public void Log(string method, string result)
        {
            string query = $"INSERT INTO LogTest (method,result) values ('{method}','{result}')";

            SqlCommand insert = new SqlCommand(query, connection);
            insert.ExecuteNonQuery();
        }
    }
}
  