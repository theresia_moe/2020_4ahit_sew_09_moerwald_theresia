﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201204_LinqStudentBook
{
    public static class StudentBook
    {
        public class Person : IEquatable<Person>
        {
            public string FName { get; set; }
            public string LName { get; set; }
            public int Age { get; set; }
            public bool Equals(Person other)
            {
                if (other is null)
                    return false;

                return this.FName == other.FName && this.LName == other.LName && this.Age == other.Age;
            }

            public override bool Equals(object obj) => Equals(obj as Person);
            public override int GetHashCode() => (FName, LName, Age).GetHashCode();
        }
        public class Student : IEquatable<Student>
        {
            public int Id { get; set; }
            public string FName { get; set; }
            public string LName { get; set; }
            public int Age { get; set; }
            public int SchoolId { get; set; }
            public bool Equals(Student other)
            {
                if (other is null)
                    return false;

                return this.Id == other.Id && this.FName == other.FName && this.LName == other.LName && this.Age == other.Age && this.SchoolId == other.SchoolId;
            }

            public override bool Equals(object obj) => Equals(obj as Student);
            public override int GetHashCode() => (Id, FName, LName, Age, SchoolId).GetHashCode();

        }
        public class School : IEquatable<School>
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public bool Equals(School other)
            {
                if (other is null)
                    return false;

                return this.Id == other.Id && this.Name == other.Name;
            }

            public override bool Equals(object obj) => Equals(obj as Student);
            public override int GetHashCode() => (Id, Name).GetHashCode();
        }
        public class Book : IEquatable<Book>
        {
            public int ID { get; set; }
            public string Title { get; set; }
            public int Pages { get; set; }
            public double Price { get; set; }
            public bool Equals(Book other)
            {
                if (other is null)
                    return false;

                return this.ID == other.ID && this.Title == other.Title && this.Pages == other.Pages && this.Price == other.Price;
            }

            public override bool Equals(object obj) => Equals(obj as Book);
            public override int GetHashCode() => (ID, Title, Pages, Price).GetHashCode();
        }
        public class Order
        {
            public int OrderId { get; set; }
            public int StudentId { get; set; }

            public int BookId { get; set; }

            public int Date { get; set; }
            public bool Equals(Order other)
            {
                if (other is null)
                    return false;

                return this.OrderId == other.OrderId && this.StudentId == other.StudentId && this.BookId == other.BookId && this.Date == other.Date;
            }

            public override bool Equals(object obj) => Equals(obj as Student);
            public override int GetHashCode() => (OrderId, StudentId, BookId, Date).GetHashCode();
        }
        public static List<Person> plist = new List<Person>
        {
            new Person {FName="Marie", LName= "Lambrecht", Age = 20 },
            new Person {FName="Paul", LName= "Stecher", Age = 32 },
            new Person {FName="Emilia", LName= "Witzel", Age = 27 },
            new Person {FName="Alexander", LName= "Brugger", Age = 23},
            new Person {FName="Anna", LName= "Pick", Age = 22},
            new Person {FName="Felix", LName= "Saarschmidt", Age = 21}
        };
        public static List<Student> slist = new List<Student>
        {
            new Student {Id=1,FName="Marie",LName="Lambrecht",Age=20, SchoolId=1},
            new Student {Id=2,FName="Paul",LName="Stecher",Age=32, SchoolId=2},
            new Student {Id=3,FName="Emilia",LName="Witzel",Age=27, SchoolId=3},
            new Student {Id=4,FName="Alexander",LName="Brugger",Age=23, SchoolId=4},
            new Student {Id=5,FName="Anna",LName="Pick",Age=22, SchoolId=5},
            new Student {Id=6,FName="Felix",LName="Saarschmidt",Age=21, SchoolId=6}
        };
        public static List<School> schlist = new List<School>
        {
            new School {Id =1, Name = "HTL Krems"},
            new School {Id =2, Name = "HAK Krems"},
            new School {Id =3, Name = "HLW Krems"},
            new School {Id =4, Name = "Gymnasium Krems"},
            new School {Id =5, Name = "HTL Zwettl"},
            new School {Id =6, Name = "HTL Zwettl"}
        };
        public static List<School> schlist2 = new List<School>
        {
            new School {Id =1, Name = "HTL Krems"},
            new School {Id =2, Name = "HAK Krems"},
            new School {Id =3, Name = "HLW Krems"},
            new School {Id =7, Name = "Gymnasium Horn"},
            new School {Id =8, Name = "HTL Wien"},
            new School {Id =9, Name = "HAK Salzburg"}
        };
        public static List<Order> olist = new List<Order>
        {
            new Order {OrderId=1, StudentId=1, BookId=1, Date=02122020},
            new Order {OrderId=2, StudentId=2, BookId=2, Date=12012020},
            new Order {OrderId=3, StudentId=3, BookId=3, Date=14102020},
            new Order {OrderId=4, StudentId=4, BookId=4, Date=23042109},
            new Order {OrderId=5, StudentId=5, BookId=5, Date=11102019},
            new Order {OrderId=6, StudentId=6, BookId=6, Date=22082019},
        };
        public static List<Book> blist = new List<Book>
        {
            new Book {ID=1,Title="Geografie", Pages=120, Price= 10.50},
            new Book {ID=2,Title="Mathematik", Pages=230, Price=12},
            new Book {ID=3,Title="Deutsch", Pages=350, Price=14.50},
            new Book {ID=4,Title="Englisch", Pages=250, Price=13},
            new Book {ID=5,Title="Physik", Pages=170, Price= 10},
            new Book {ID=6,Title="Wirtschaft", Pages=210, Price=11.50}
        };
        public class StudentOrderReport
        {
            public int Date { get; set; }
            public string StudentName { get; set; }
        }
        public class BookOrderedReport
        {
            public int Date { get; set; }
            public string BookTitle { get; set; }
        }
    }
}
