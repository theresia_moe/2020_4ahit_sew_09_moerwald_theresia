﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using _201204_LinqStudentBook;
using static _201204_LinqStudentBook.StudentBook;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace _201204_LinqStudentBookTest
{
    [TestClass]
    public class UnitTest1
    {
        //ILogger logger = new TxtLogger("testlog.txt");

        //ILogger logger = new CsvLogger("testlog.csv");
        //ILogger logger = new MDFLogger();
         ILogger logger = new XMLLogger("testlog.xml");
       

        [TestMethod]
        public void AverageStudentAgeTest()
        {
            double sum = 0;
            double average = 0;
            double averageLinq = LinqMethods.AverageStudentAge(slist);

            foreach (Student s in slist)
            {
                sum += s.Age;
            }
            average = sum / slist.Count;      

            try
            {
                Assert.AreEqual(average, averageLinq);
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                //Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");

            }
        }
        [TestMethod]
        public void SumBookPagesTest()
        {
            int sum = 0;
            int sumLinq = LinqMethods.SumBookPages(blist);

            foreach (Book b in blist)
            {
                sum += b.Pages;
            }
                     
            try
            {
                Assert.AreEqual(sum, sumLinq);
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");               
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
            }
        }

        [TestMethod]
        public void CountStudentsTest()
        {
            int studentcount = slist.Count;
            int studentcountLinq = LinqMethods.CountStudents(slist);

            
            try
            {
                Assert.AreEqual(studentcount, studentcountLinq);
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");

            }

        }

        [TestMethod]
        public void CheapestBookTest()
        {
            List<Book> cheapestBookLinq = LinqMethods.CheapestBook(blist);

            List<Book> cheapestBook = new List<Book>();

            double price = blist[0].Price;

            foreach (Book b in blist)
            {
                if (b.Price < price)
                    price = b.Price;
            }
            foreach (Book b in blist)
            {
                if (price == b.Price)
                    cheapestBook.Add(b);

            }
            cheapestBook.Sort();
            cheapestBookLinq.Sort();

            try
            {

                for (int i = 0; i < cheapestBook.Count; i++)
                {
                    Assert.AreEqual(cheapestBook[i].Title, cheapestBookLinq[i].Title);
                    Assert.AreEqual(cheapestBook[i].Pages, cheapestBookLinq[i].Pages);
                    Assert.AreEqual(cheapestBook[i].Price, cheapestBookLinq[i].Price);
                }
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
            }

        }
        [TestMethod]
        public void MostExpensiveBookTest()
        {
            List<Book> mostExpBookLinq = LinqMethods.MostExpensiveBook(blist);

            List<Book> mostExpBook = new List<Book>();

            double price = blist[0].Price;

            foreach (Book b in blist)
            {
                if (b.Price > price)
                    price = b.Price;
            }
            foreach (Book b in blist)
            {
                if (price == b.Price)
                    mostExpBook.Add(b);

            }
            mostExpBook.Sort();
            mostExpBookLinq.Sort();
          
            try
            {

                for (int i = 0; i < mostExpBook.Count; i++)
                {
                    Assert.AreEqual(mostExpBook[i].Title, mostExpBookLinq[i].Title);
                    Assert.AreEqual(mostExpBook[i].Pages, mostExpBookLinq[i].Pages);
                    Assert.AreEqual(mostExpBook[i].Price, mostExpBookLinq[i].Price);
                }
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
            }

        }
        [TestMethod]
        public void OrderByBookPages()
        {
            List<Book> sortedList = new List<Book>
            {
                new Book {Title="Geografie", Pages=120, Price= 10.50},
                new Book {Title="Physik", Pages=170, Price= 10},
                new Book {Title="Wirtschaft", Pages=210, Price=11.50},
                new Book {Title="Mathematik", Pages=230, Price=12},
                new Book {Title="Englisch", Pages=250, Price=13},
                new Book {Title="Deutsch", Pages=350, Price=14.50}
            };
            List<Book> sortedListLinq = LinqMethods.OrderByBookPages(blist);

            try
            {
                for (int i = 0; i < sortedList.Count; i++)
                {
                    Assert.AreEqual(sortedList[i].Title, sortedListLinq[i].Title);
                    Assert.AreEqual(sortedList[i].Pages, sortedListLinq[i].Pages);
                    Assert.AreEqual(sortedList[i].Price, sortedListLinq[i].Price);
                }
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
            }

        }
        [TestMethod]
        public void OrderByNameThenIDTest()
        {
            Dictionary<int, string> sorteddict = new Dictionary<int, string>();
            sorteddict.Add(4, "Gymnasium Krems");
            sorteddict.Add(2, "HAK Krems");
            sorteddict.Add(3, "HLW Krems");
            sorteddict.Add(1, "HTL Krems");
            sorteddict.Add(5, "HTL Zwettl");
            sorteddict.Add(6, "HTL Zwettl");
            Dictionary<int, string> sorteddictLinq = LinqMethods.OrderByNameThenID(schlist);

            try
            {
                for (int i = 1; i <= schlist.Count; i++)
                {
                    Assert.AreEqual(sorteddict[i], sorteddictLinq[i]);

                }
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
            }
        }

        [TestMethod]
        public void OfTypeStringTest()
        {
            List<object> objectSchoolList = new List<object>();

            foreach (School s in schlist)
            {
                objectSchoolList.Add(s);
            }
            School[] ofTypArray = LinqMethods.OfTypeSchool(objectSchoolList);

           
            try
            {
                Assert.AreEqual(ofTypArray.Length, objectSchoolList.Count);
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
            }


        }

        [TestMethod]
        public void OrderbyPagesDescendingSkipTest()
        {
            List<Book> sortedDescList = new List<Book>
            {
                new Book {Title="Mathematik", Pages=230, Price=12},
                new Book {Title="Wirtschaft", Pages=210, Price=11.50},
                new Book {Title="Physik", Pages=170, Price= 10},
                new Book {Title="Geografie", Pages=120, Price= 10.50},

            };
            List<Book> sortedDescListLinq = LinqMethods.OrderbyPagesDescendingSkip(blist, 2);                    

            try
            {
                for (int i = 0; i < sortedDescList.Count; i++)
                {
                    Assert.AreEqual(sortedDescList[i].Title, sortedDescListLinq[i].Title);
                    Assert.AreEqual(sortedDescList[i].Pages, sortedDescListLinq[i].Pages);
                    Assert.AreEqual(sortedDescList[i].Price, sortedDescListLinq[i].Price);
                }
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
            }
        }
        [TestMethod]
        public void ReverseStudentNamesTest()
        {
            List<Student> reverseList = new List<Student>
              {
                 new Student {Id=6,FName="Felix",LName="Saarschmidt",Age=21, SchoolId=6},
                 new Student {Id=5,FName="Anna",LName="Pick",Age=22, SchoolId=5},
                 new Student {Id=4,FName="Alexander",LName="Brugger",Age=23, SchoolId=4},
                 new Student {Id=3,FName="Emilia",LName="Witzel",Age=27, SchoolId=3},
                 new Student {Id=2,FName="Paul",LName="Stecher",Age=32, SchoolId=2},
                 new Student {Id=1,FName="Marie",LName="Lambrecht",Age=20, SchoolId=1 }

              };

            List<Student> reverseListLinq = LinqMethods.ReverseStudentNames(slist);

           
            try
            {
                for (int i = 0; i < slist.Count; i++)
                {
                    Assert.AreEqual(reverseList[i].Id, reverseListLinq[i].Id);
                }
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");

            }
        }

        [TestMethod]
        public void TakeNumberOfSchoolTest()
        {

            List<School> firstFourschoolLinq = LinqMethods.TakeNumberOfSchool(schlist, 4);
                       
            try
            {
                Assert.AreEqual(4, firstFourschoolLinq.Count);
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
            }

        }

        [TestMethod]
        public void ConcatSchoolsTest()
        {
            List<School> schoolconcated = LinqMethods.ConcatSchools(schlist, schlist2);
            
            try
            {
                Assert.AreEqual(schoolconcated.Count, schlist.Count + schlist2.Count);
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
            }

        }

        [TestMethod]
        public void RemoveRedundantSchoolsTest()
        {
            List<School> schoolconcated = LinqMethods.ConcatSchools(schlist, schlist2);
            List<School> resultLinq = LinqMethods.RemoveRedundantSchools(schoolconcated);

            List<School> result = new List<School>
                {
                    new School {Id =1, Name = "HTL Krems"},
                    new School {Id =2, Name = "HAK Krems"},
                    new School {Id =3, Name = "HLW Krems"},
                    new School {Id =4, Name = "Gymnasium Krems"},
                    new School {Id =5, Name = "HTL Zwettl"},
                    new School {Id =6, Name = "HTL Zwettl"},
                    new School {Id =7, Name = "Gymnasium Horn"},
                    new School {Id =8, Name = "HTL Wien"},
                    new School {Id =9, Name = "HAK Salzburg"}
                };

            result.OrderBy(s => s.Id);
            resultLinq.OrderBy(s => s.Id);
                        
            try
            {
                for (int i = 0; i < result.Count; i++)
                {
                    Assert.AreEqual(result[i].Id, resultLinq[i].Id);
                    Assert.AreEqual(result[i].Name, resultLinq[i].Name);
                }
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
            }
        }

        [TestMethod]
        public void ExpetSchoolsTest()
        {
            List<School> resultLinq = LinqMethods.ExpectSchools(schlist, schlist2);

            List<School> result = new List<School>
                {
                    new School {Id =4, Name = "Gymnasium Krems"},
                    new School {Id =5, Name = "HTL Zwettl"},
                    new School {Id =6, Name = "HTL Zwettl"}
                };

            try
            {
                for (int i = 0; i < result.Count; i++)
                {
                    Assert.AreEqual(result[i].Id, resultLinq[i].Id);
                    Assert.AreEqual(result[i].Name, resultLinq[i].Name);
                }
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");

            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");

            }
        }

        [TestMethod]
        public void IntersectSchoolsTest()
        {

            List<School> result = new List<School>
            {
                new School { Id = 1, Name = "HTL Krems" },
                new School { Id = 2, Name = "HAK Krems" },
                new School { Id = 3, Name = "HLW Krems" }
            };

            List<School> resultLinq = LinqMethods.IntersectSchools(schlist, schlist2);
            try
            {
                for (int i = 0; i < result.Count; i++)
                {
                    Assert.AreEqual(result[i].Id, resultLinq[i].Id);
                    Assert.AreEqual(result[i].Name, resultLinq[i].Name);
                }
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
            }
        }

        [TestMethod]
        public void UnionSchoolTest()
        {

            List<School> concated = LinqMethods.ConcatSchools(schlist, schlist2);
            List<School> result = LinqMethods.RemoveRedundantSchools(concated);
            List<School> resultLinq = LinqMethods.UnionSchools(schlist, schlist2);

            Assert.AreEqual(9, resultLinq.Count);

            try
            {
                for (int i = 0; i < result.Count; i++)
                {
                    Assert.AreEqual(result[i].Id, resultLinq[i].Id);
                    Assert.AreEqual(result[i].Name, resultLinq[i].Name);
                }
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "succeed");
            }
            catch
            {
                Assert.Fail();
                logger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
            }
        }
    }



}





