﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace _201009_PersonalDetail
{
    class PersonalDetails
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string firstname;
        private string lastname;
        private int age;

        public string FullName { get; set; }

        public string FirstName
        {
            get { return firstname; }
            set
            {
                firstname = value;
                FullName = firstname + " " + lastname;
                OnPropertyChanged("FullName");

            }

        }
        public string LastName
        {
            get { return lastname; }
            set
            {
                lastname = value;
                FullName = firstname + " " + lastname;
                OnPropertyChanged("FullName");


            }

        }
        public int Age
        {
            get { return this.age; }
            set
            {
                age = value;
                OnPropertyChanged("Age");
            }
        }
        protected virtual void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
