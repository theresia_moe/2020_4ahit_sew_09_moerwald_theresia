﻿using System;
using System.ComponentModel.DataAnnotations;

namespace _20210426_ASP.Models {
    public class Foo {
        public int Id { get; set; }
        
        [Required]
        [Range(1, 100)]
        public double Height { get; set; }
        
        [DataType(DataType.Date)]
        [Required]
        public DateTime CreatedDate { get; set; }
        
        [RegularExpression(@"^[A-Z]+[a-zA-Z\s]*$")]
        [StringLength(45)]
        [Required]
        public string Title { get; set; }
        
    }
}