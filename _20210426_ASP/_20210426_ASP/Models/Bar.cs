﻿namespace _20210426_ASP.Models {
    public class Bar {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsBar { get; set; }
    }
}