﻿using Microsoft.EntityFrameworkCore;

namespace _20210426_ASP.Models {
    public class FoobarDBContext : DbContext {
        
        public DbSet<Foo> Foos { get; set; }
        public DbSet<Bar> Bars { get; set; }

        public FoobarDBContext() {
        }

        public FoobarDBContext(DbContextOptions<FoobarDBContext> options) : base(options) {
            
        }
    }
}