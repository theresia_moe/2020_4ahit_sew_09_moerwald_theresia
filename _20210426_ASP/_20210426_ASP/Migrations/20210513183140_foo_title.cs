﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace _20210426_ASP.Migrations
{
    public partial class foo_title : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Foos",
                type: "longtext",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Title",
                table: "Foos");
        }
    }
}
