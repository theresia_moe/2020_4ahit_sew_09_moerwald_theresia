﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;


namespace _201015_WPFGreet
{
    class GreetCommand : ICommand
    {
       
        GreetVM vm;

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
        public GreetCommand(GreetVM vm)
        {
            this.vm = vm;
        }
        public bool CanExecute(object parameter)
        {
            return (vm.Name != null && vm.Name != "");
        }

        public void Execute(object parameter)
        {
            MessageBox.Show("Hallo " + vm.Name);
        }
    }
}
