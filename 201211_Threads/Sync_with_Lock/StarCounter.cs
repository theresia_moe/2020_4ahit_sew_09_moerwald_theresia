﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sync_with_Lock
{
    static public class StarCounter
    {
        private static int counter;
        static object locker = new object();

        public static void PrintStar()
        {
            lock (locker)
            {
                for (counter = 0; counter < 5; counter++)
                {
                    Console.Write(" * " + "\t");
                }
            }
        }

        public static void PrintPlus()
        {
            lock (locker)
            {
                for (counter = 0; counter < 5; counter++)
                {
                    Console.Write(" + " + "\t");
                }
            }
        }
    }
}
