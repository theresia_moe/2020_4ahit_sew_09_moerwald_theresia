﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Race_Condition
{
    class Program
    {
        //Race Condition Star & Plus
        static void Main(string[] args)
        {
            Thread T1 = new Thread(StarCounter.PrintStar);
            T1.Start();

            Thread T2 = new Thread(StarCounter.PrintPlus);
            T2.Start();

            Console.ReadLine();
        }
    }
}
