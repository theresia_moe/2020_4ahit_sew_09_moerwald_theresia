﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Race_Condition
{
    static public class StarCounter
    {
        private static int counter;

        public static void PrintStar()
        {
            for(counter = 0; counter < 5; counter++)
            {
                Console.Write(" * " + "\t");
            }
        }

        public static void PrintPlus()
        {
            for(counter = 0; counter < 5; counter++)
            {
                Console.Write(" + " + "\t");
            }
        }
    }
}
