﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Threads_mit_Parameter
{
    class Program
    {
        //Threads mit Parameter
        static void Main(string[] args)
        {
            ParameterizedThreadStart pts = new ParameterizedThreadStart(Method);
            Thread t = new Thread(pts);
            t.Start(43);
            //or
            Thread t2 = new Thread(delegate () { Method("hello thread"); });
            t2.Start();

            Console.ReadLine();
        }

        private static void Method(Object parameter)
        {
            Console.WriteLine(parameter);
        }
    }
}
