﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sync_with_Join
{
    class Program
    {
        //Synchronization mit Thread.Join()
        static void Main(string[] args)
        {
            Thread T1 = new Thread(StarCounter.PrintStar);
            T1.Start();
            T1.Join();
            Console.WriteLine();
            Thread T2 = new Thread(StarCounter.PrintPlus);
            T2.Start();
            T2.Join();

            Console.WriteLine("Ending main thread");

            Console.ReadLine();
        }
    }
}
