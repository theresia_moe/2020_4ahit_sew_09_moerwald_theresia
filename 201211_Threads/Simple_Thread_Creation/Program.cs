﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Simple_Thread_Creation
{
    class Program
    {
        //Simple Thread Creation
        static void Main(string[] args)
        {
            Thread t = new Thread(myFun);
            t.Start();

            Console.WriteLine("Main thread Running");
            Console.ReadLine();
        }
        static void myFun()
        {
            Console.WriteLine("Running other Thread");
        }
    }
}
