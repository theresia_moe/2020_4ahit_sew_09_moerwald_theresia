﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sync_with_Monitor
{
    static public class StarCounter
    {
        private static int counter;
        static object locker = new object();

        public static void PrintStar()
        {
            Monitor.Enter(locker);
            try { 

                for (counter = 0; counter < 5; counter++)
                {
                    Console.Write(" * " + "\t");
                }
            }
            finally
            {
                Monitor.Exit(locker);
            }
        }

        public static void PrintPlus()
        {
            Monitor.Enter(locker);
            try
            {

                for (counter = 0; counter < 5; counter++)
                {
                    Console.Write(" + " + "\t");
                }
            }
            finally
            {
                Monitor.Exit(locker);
            }
        }
    }
}
