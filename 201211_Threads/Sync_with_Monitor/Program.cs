﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sync_with_Monitor
{
    class Program
    {
        //Synchronization mit Monitor
        static void Main(string[] args)
        {
            Thread T1 = new Thread(StarCounter.PrintStar);
            Thread T2 = new Thread(StarCounter.PrintPlus);

            T1.Start();
            T2.Start();

            Console.ReadLine();

        }
    }
}
