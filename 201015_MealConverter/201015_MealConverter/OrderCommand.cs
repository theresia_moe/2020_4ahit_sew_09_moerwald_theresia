﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace _201015_MealConverter
{
    class OrderCommand : ICommand
    {
        SliderVM parent;

        public OrderCommand(SliderVM vm)
        {
            parent = vm;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            MessageBox.Show(parent.Order());
        }
    }
}
