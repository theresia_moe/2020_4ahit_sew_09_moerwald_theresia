﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _201015_MealConverter
{
    enum EWeekDays { MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY }
    class SliderVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand OrderCommand { get; }
        public SliderVM()
        {
            OrderCommand = new OrderCommand(this);
        }
        EWeekDays eWeekDay;
        public EWeekDays EWeekDay
        {
            get
            {
                return eWeekDay;
            }
            set
            {
                eWeekDay = value;
                OnPropertyChange();
            }
        }
        bool breakfast;
        bool lunch;
        bool dinner;
        public bool Breakfast
        {
            get
            {
                return breakfast;
            }
            set
            {
                breakfast = value;
                Food = GetFoodString();
                OnPropertyChange();
            }
        }
        public bool Lunch
        {
            get
            {
                return lunch;
            }
            set
            {
                lunch = value;
                Food = GetFoodString();
                OnPropertyChange();

            }
        }
        public bool Dinner
        {
            get
            {
                return dinner;
            }
            set
            {
                dinner = value;
                Food = GetFoodString();
                OnPropertyChange();
            }
        }

        string food;
        public string Food
        {
            get
            {
                return food;
            }
            set
            {
                food = value;
                OnPropertyChange();
            }
        }

        void OnPropertyChange([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public string Order()
        {

            return GetFoodString() + "\n" + eWeekDay.ToString();

        }
        string GetFoodString()
        {

            if (breakfast && lunch && dinner)
            {
                return "Breakfast \n Lunch\n Dinner";
            }
            else if (!breakfast && lunch && dinner)
            {
                return " Lunch\n Dinner";
            }
            else if (!breakfast && !lunch && dinner)
            {
                return " Dinner";
            }
            else if (breakfast && !lunch && dinner)
            {
                return " Breakfast\n Dinner";
            }
            else if (breakfast && lunch && !dinner)
            {
                return " Breakfast\n Lunch";
            }
            else if (!breakfast && lunch && !dinner)
            {
                return " Lunch";
            }
            else if (breakfast && !lunch && !dinner)
            {
                return " Breakfast";
            }
            else
            {
                return "";
            }
        }
    }
}

