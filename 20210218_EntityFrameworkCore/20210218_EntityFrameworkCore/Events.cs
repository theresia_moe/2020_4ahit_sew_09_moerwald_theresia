﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20210218_EntityFrameworkCore
{
    class Events
    {
        public int Id { get; set; }
        public Room Room { get; set; }

        public List<People> Participants { get;set; }

        public char Wing { get; set; }

        public short Duration { get; set; }

        public void ToString() { }
        public void ToCSV() { }
        public void ReadCSV() { }
    }
}
