﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20210218_EntityFrameworkCore
{
    class Student : People
    {
        public int MatriculationNumber { get; set; }
        public ESubject[] subjects { get; set; }


        public void ToString() { }
        public void ToCSV() { }
        public void ReadCSV() { }
    }

    public enum ESubject
    {
        Educationalscience,
        Politicalscience,
        Psychology,
        Sociology,
        Sports,
        Theology,
        BigData
    }
}
