﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20210218_EntityFrameworkCore
{
    class Room
    {
        public int Id { get; set; }
        public char Wing { get; set; }

        public ERoomType RoomType { get; set; }



        public void ToString() { }
        public void ToCSV() { }
        public void ReadCSV() { }
    }

    public enum ERoomType
    {
        EDVRoom,
        LectureRoom,
        Laboratory
    }
}
