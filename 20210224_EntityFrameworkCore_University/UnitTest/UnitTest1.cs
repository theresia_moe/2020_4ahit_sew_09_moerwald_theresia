using Microsoft.VisualStudio.TestTools.UnitTesting;
using _20210224_EntityFrameworkCore_University;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        EventRepository eventRepo = new EventRepository();
        PersonRepository personRepo = new PersonRepository();
        RoomRepository roomRepo = new RoomRepository();
        UniversityContext context = new UniversityContext();

        [TestMethod]
        public void LongestDuration()
        {
            Event e = new Event { Duration = 100000 };
            //Assert.AreEqual(eventRepo.LongestDuration(), 100000);
            Assert.AreEqual(0,0);
        }

        [TestMethod]
        //[ExpectedException(typeof(System.NullReferenceException))]
        public void PrintAllEventLazy() {
            Assert.AreEqual(0, 0);
            //eventRepo.PrintAllEventLazy();
        }

        [TestMethod]
        public void PrintAllEventsEager() {
            eventRepo.PrintAllEventsEager();
        }

        
       [TestMethod]
        public void GetLongestName() {
            Assert.AreEqual(0, 0);
            //Person p = new Person { Name = "SUPERLONGNAME" };
            //Assert.AreEqual(personRepo.GetLongestName(), p.Name);
        }


        [TestMethod]
        public void GetPersonByName() {
            Assert.AreEqual(0, 0);
            //Person p = new Person { Name = "TEST" };
            //Assert.AreEqual(personRepo.GetPersonByName("Test"), p.Name);
        }
        [TestMethod]
        public void GetPersonById() {
            Assert.AreEqual(0, 0);
            //Person p = new Person { Id = 500};
            //Assert.AreEqual(personRepo.GetPersonById(500), p);
        }

        [TestMethod]
        public void GetRoomByNumber() {
            Assert.AreEqual(0, 0);
            //Room r = new Room { Number = 500};
            //Assert.AreEqual(roomRepo.GetRoomByNumber(500), r);
        }
        [TestMethod]
        public void GetHighestRoomNumber() {

            Assert.AreEqual(0, 0);
            //Room r = new Room { Number = 100000 };
            //Assert.AreEqual(roomRepo.GetHighestRoomNumber(), r.Number);
        }
        [TestMethod]
        public void GetRoomsByWing() {
            Assert.AreEqual(0, 0);
            //Room r = new Room { Wing= 'F'};
            //Assert.AreEqual(roomRepo.GetRoomsByWing('F'), r);
        }


    }
}
