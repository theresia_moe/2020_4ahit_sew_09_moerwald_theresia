﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _20210224_EntityFrameworkCore_University
{
    public class RoomRepository : ARepository<Room, int>
    {
        protected override DbSet<Room> Table { get; }

        public List<Room> GetRoomByNumber(int number)
        {
            return Context.Rooms.Where(r => r.Number == number).ToList();
        }

        public int GetHighestRoomNumber()
        {
            return Context.Rooms.Max(r => r.Number);
        }

        public List<Room> GetRoomsByWing(char wing)
        {
            return Context.Rooms.Where(r => r.Wing == wing).ToList();
        }


    }


}
