﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;

namespace _20210224_EntityFrameworkCore_University
{
    class Program
    {
        private static void DisplayStates(IEnumerable<EntityEntry> entries)
        {
            foreach (var entry in entries)
            {
                Console.WriteLine($"Entity: {entry.Entity.GetType().Name},State: { entry.State.ToString()}");
            }
        }
        static void Main(string[] args)
        {
            using (var context = new UniversityContext())
            {
                var st1 = new StudyPath() { Description = "IT" };
                var st2 = new StudyPath() { Description = "Marketing" };

                var p1 = new Person() { Name = "Theresia", StudyPath = st1};
                var p2 = new Person() { Name = "Johanna", StudyPath = st1 };
                var p3 = new Person() { Name = "Antonia", StudyPath = st2 };

                var r1 = new Room() { Number = 50, Wing = 'A' };
                var r2 = new Room() { Number = 65, Wing = 'C' };
                var r3 = new Room() { Number = 110, Wing = 'E' };


                var e1 = new Event() { Name ="Vorlesung Wirtschaftsrecht", Duration = 3, Room = r1};
                var e2 = new Event() { Name = "Prüfung Rechnungswesen", Duration = 1, Room = r2 };
                var e3 = new Event() { Name = "Vorlesung Softwareentwicklung", Duration = 2, Room = r3 };

                context.StudyPaths.Add(st1);
                context.StudyPaths.Add(st2);
                context.Persons.Add(p1);
                context.Persons.Add(p2);
                context.Persons.Add(p3);
                context.Rooms.Add(r1);
                context.Rooms.Add(r2);
                context.Rooms.Add(r3);
                context.Events.Add(e1);
                context.Events.Add(e2);
                context.Events.Add(e3);



                context.SaveChanges();
                DisplayStates(context.ChangeTracker.Entries());


            }

        }
    }
}
