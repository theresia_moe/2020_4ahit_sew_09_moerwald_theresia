﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _20210224_EntityFrameworkCore_University
{
    public  class EventRepository : ARepository<Event, int>
    {
        protected override DbSet<Event> Table { get; }

        public double LongestDuration()
        {
            return Context.Events.Max(e => e.Duration);
        }

        public void PrintAllEventLazy()
        {
            IList<Event> events = Context.Events.ToList<Event>();
            foreach (var e in events)
            {
                Console.WriteLine(e.Name);
            }
            foreach (var e in events)
            {
                Console.WriteLine(e.Room.Id);
            }
        }

        public void PrintAllEventsEager()
        {
            IList<Event> events = Context.Events
               .Include(r => r.Room)
               .ToList<Event>();
            foreach (var e in events)
            {
                Console.WriteLine(e.Name);
            }
            foreach (var e in events)
            {
                Console.WriteLine(e.Room.Id);
            }
        }
    }
}
