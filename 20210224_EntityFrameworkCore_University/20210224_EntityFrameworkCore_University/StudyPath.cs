﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _20210224_EntityFrameworkCore_University
{
    public class StudyPath
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
