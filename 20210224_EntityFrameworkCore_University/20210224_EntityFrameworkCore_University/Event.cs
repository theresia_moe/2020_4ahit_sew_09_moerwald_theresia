﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace _20210224_EntityFrameworkCore_University
{
    public class Event
    {
        public int Id { get; set; }

        public double Duration { get; set; }
        public string Name { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public IList<Person> Participants { get; set; } = new List<Person>();

        public Room Room { get; set; }


    }

    public class Lecture : Event { }
    public class Exam : Event { }
    public class ProjectWork : Event { }
}
