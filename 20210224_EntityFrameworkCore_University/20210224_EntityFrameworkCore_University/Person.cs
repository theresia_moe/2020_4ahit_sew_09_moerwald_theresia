﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace _20210224_EntityFrameworkCore_University
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public IList<Event> Events { get; set; } = new List<Event>();

        public StudyPath StudyPath { get; set; }


    }

        [Table("Student")]
        public class Student : Person
        {
            public string MatrikelNr { get; set; }
        }

        [Table("Professor")]
        public class Professor : Person
        {
            public string Title { get; set; }
        }

        [Table("Assistent")]
        public class Assistent : Person
        {
            public double Salary { get; set; }
        }
    }
