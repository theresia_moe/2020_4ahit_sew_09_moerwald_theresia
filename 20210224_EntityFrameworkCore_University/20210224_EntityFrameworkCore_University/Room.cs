﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace _20210224_EntityFrameworkCore_University
{
    public class Room
    {
        public int Id { get; set; }
        public char Wing { get; set; }
        public int Number { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}
