﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _20210224_EntityFrameworkCore_University
{
    public abstract class ARepository<T, TId> : IDisposable
          where T : class
    {
        protected UniversityContext Context { get; }
        protected abstract DbSet<T> Table { get; }
        public ARepository()
        {
            Context = new UniversityContext();
        }
        public ARepository(UniversityContext context)
        {
            this.Context = context;
        }
        public List<T> GetAll() => Table.ToList();
        public T GetOne(TId id) => Table.Find(id);
        public int Add(T entity)
        {
            Table.Add(entity);
            return Context.SaveChanges();
        }
        public int AddRange(IEnumerable<T> entities)
        {
            Table.AddRange(entities);
            return Context.SaveChanges();
        }
        public int Delete(T entity)
        {
            Table.Remove(entity);
            return Context.SaveChanges();
        }
        public int DeleteRange(IEnumerable<T> entities)
        {
            Table.RemoveRange(entities);
            return Context.SaveChanges();
        }
        public int Update() => Context.SaveChanges();
        public void Dispose() => Context.Dispose();
    }
}
