﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _20210224_EntityFrameworkCore_University
{
    public class PersonRepository : ARepository<Person, int>

    {
        protected override DbSet<Person> Table { get; }

        public string GetLongestName()
        {
            return Context.Persons.Max(p => p.Name);
        }

        public List<Person> GetPersonByName(string name)
        {
            return Context.Persons.Where(p => p.Name == name).ToList();
        }

        public List<Person> GetPersonById(int id)
        {
            return Context.Persons.Where(p => p.Id == id).ToList();
        }

    }
}
