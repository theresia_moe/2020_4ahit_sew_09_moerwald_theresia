﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;


namespace _20210224_EntityFrameworkCore_University
{
    public class UniversityContext:DbContext
    {
        public static readonly ILoggerFactory loggerFactory =
        LoggerFactory.Create(builder => { builder.AddConsole(); });


        public UniversityContext()
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        { 
            optionsBuilder
                .UseLoggerFactory(loggerFactory)
                .EnableSensitiveDataLogging()
                .UseSqlServer(
                @"Server=(localdb)\MSSQLLocalDB;
                Database= advanced;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Event>().Property<DateTime>("CreatedDate");
            modelBuilder.Entity<Event>().Property<DateTime>("UpdatedDate");

            modelBuilder.Entity<Person>().Property<DateTime>("CreatedDate");
            modelBuilder.Entity<Person>().Property<DateTime>("UpdatedDate");

            modelBuilder.Entity<Room>().Property<DateTime>("CreatedDate");
            modelBuilder.Entity<Room>().Property<DateTime>("UpdatedDate");


        }

        public override int SaveChanges()
        {
            ChangeTracker.DetectChanges();

            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.State == EntityState.Added)
                    entry.Property("CreatedDate").CurrentValue = DateTime.UtcNow;
                else if (entry.State == EntityState.Modified)
                {
                    entry.Property("UpdatedDate").CurrentValue = DateTime.UtcNow;
                }
            }

            return base.SaveChanges();
        }


       
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<StudyPath> StudyPaths { get; set; }


        public DbSet<Person> Persons { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Assistent> Assistens { get; set; }

        public DbSet<Event> Events { get; set; }
        public DbSet<Lecture> Lectures { get; set; }
        public DbSet<Exam> Exams { get; set; }
        public DbSet<ProjectWork> ProjektWorks { get; set; }

    }
}
