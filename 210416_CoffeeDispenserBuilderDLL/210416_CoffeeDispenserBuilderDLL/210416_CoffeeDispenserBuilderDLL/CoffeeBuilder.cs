﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210416_CoffeeDispenserBuilderDLL
{
    public  class CoffeeBuilder : ABeverageBuilder
    {

        public override void CreateBeverage()
        {
            beverage = new BlackCoffee();
        }

        public override void SetWater()
        {
            GetBeverage().Water = 50;
        }

        public override void SetMilk()
        {
            GetBeverage().Milk = 0;
        }

        public override void SetPowderQuantity()
        {
            GetBeverage().Sugar = 0;
        }

        public override void SetSugar()
        {
            GetBeverage().PowderQuantity = 40;
        }

     

        public override void SetBeverageType()
        {
 
            GetBeverage().BeverageName = "Schwazer Kafee";
        }
    }
}
