﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210416_CoffeeDispenserBuilderDLL
{
    public class TeaWithMilk : ABeverage
    {
        public TeaWithMilk()
        {
            GetPrice();
        }

        public override string ToString()
        {
            StringBuilder text = new StringBuilder("Tee mit Milch");           
            return text.ToString();
        }
    }
}
