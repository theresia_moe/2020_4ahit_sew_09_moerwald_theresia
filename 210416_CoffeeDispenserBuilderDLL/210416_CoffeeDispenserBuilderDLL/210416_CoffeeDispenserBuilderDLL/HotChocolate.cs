﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210416_CoffeeDispenserBuilderDLL
{
    public class HotChocolate : ABeverage
    {
        public HotChocolate()
        {
            GetPrice();
        }

        public override string ToString()
        {
            StringBuilder text = new StringBuilder("Heiße Schokolade");
            return text.ToString();
        }
    }
}
