﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210416_CoffeeDispenserBuilderDLL
{
    public class TeaBuilder : ABeverageBuilder
    {
        public override void CreateBeverage()
        {
            beverage = new TeaWithMilk();
        }

        public override void SetWater()
        {
            GetBeverage().Water = 50;
        }

        public override void SetMilk()
        {
            GetBeverage().Milk = 20;
        }

        public override void SetPowderQuantity()
        {
            GetBeverage().Sugar = 10;
        }

        public override void SetSugar()
        {

            GetBeverage().PowderQuantity = 20;
        }

        public override void SetBeverageType()
        {
            GetBeverage().BeverageName = "Tea mit Milch";
        }
    }
}
