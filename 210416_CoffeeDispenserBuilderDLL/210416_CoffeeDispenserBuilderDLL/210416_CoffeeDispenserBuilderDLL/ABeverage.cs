﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210416_CoffeeDispenserBuilderDLL
{
    public abstract class ABeverage
    {

        public int Water { get; set; }
        public int Milk { get; set; }
        public int Sugar { get; set; }
        public int PowderQuantity { get; set; }
        public string BeverageName { get; set; }


        public override string ToString()
        {
            return "\n\t Hot" + BeverageName + "\n\t "
                + Water + " ml of water\n\t "
                + Milk + " gm of milk\n\t "
                + Sugar + " gm of sugar\n\t "
                + PowderQuantity + " gm of "
                + BeverageName + "\n";
        }

        public double GetPrice()
        {
            return 0.5 + 0.1 + 0.1;
        }
    }
}
