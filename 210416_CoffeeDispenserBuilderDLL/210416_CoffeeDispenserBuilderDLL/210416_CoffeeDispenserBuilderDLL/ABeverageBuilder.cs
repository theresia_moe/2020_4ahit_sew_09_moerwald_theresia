﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210416_CoffeeDispenserBuilderDLL
{
    public abstract class ABeverageBuilder
    {
        protected ABeverage beverage;

        public ABeverage GetBeverage() {
            return beverage;
        }

        public abstract void CreateBeverage();
        public abstract void SetBeverageType();
        public abstract void SetWater();
        public abstract void SetMilk();
        public abstract void SetSugar();
        public abstract void SetPowderQuantity();
    }
}
