﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using _210416_CoffeeDispenserBuilderDLL;

namespace _210416_CoffeeDispenserBuilderTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestBlackCoffeeBuilder()
        {
            CoffeeBuilder coffee = new CoffeeBuilder();
            BeverageDirector beverageDirector = new BeverageDirector();
            ABeverage bev;

            bev = beverageDirector.MakeBeverage(coffee);


            Assert.IsTrue(bev.GetType() == typeof(BlackCoffee) && bev.Water == 50 && bev.Milk == 0 && bev.Sugar==0 && bev.PowderQuantity==40);
        }

        [TestMethod]
        public void TestHotChocolate()
        {
            HotChocolateBuilder hotChocolate = new HotChocolateBuilder();
            BeverageDirector beverageDirector = new BeverageDirector();
            ABeverage bev;

            bev = beverageDirector.MakeBeverage(hotChocolate);


            Assert.IsTrue(bev.GetType() == typeof(HotChocolate) && bev.Water == 50 && bev.Milk == 20 && bev.Sugar == 10 && bev.PowderQuantity == 20);
        }

        [TestMethod]
        public void TestTeaWithMilk()
        {
            TeaBuilder tea = new TeaBuilder();
            BeverageDirector beverageDirector = new BeverageDirector();
            ABeverage bev;

            bev = beverageDirector.MakeBeverage(tea);


            Assert.IsTrue(bev.GetType() == typeof(TeaWithMilk) && bev.Water == 50 && bev.Milk == 20 && bev.Sugar == 10 && bev.PowderQuantity == 20);
        }

    }
}
