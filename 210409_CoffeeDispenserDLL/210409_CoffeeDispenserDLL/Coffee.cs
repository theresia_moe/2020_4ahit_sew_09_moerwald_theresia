﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210409_CoffeeDispenserDLL
{
    class Coffee : Beverage
    {
        public Coffee(EMilk milk, ESugar sugar) : base(milk, sugar)
        {
            SetPrice(2);
            Type = EBeverageType.COFFEE;
        }

        public override string ToString()
        {
            StringBuilder text = new StringBuilder("Kaffee");
            text.Append(base.MilkSugarInfo());
            return text.ToString();
        }
    }
}
