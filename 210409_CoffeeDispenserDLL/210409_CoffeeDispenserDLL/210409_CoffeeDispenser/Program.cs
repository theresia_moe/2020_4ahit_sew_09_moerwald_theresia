﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _210409_CoffeeDispenserDLL;

namespace _210409_CoffeeDispenser
{
    class Program
    {
        static void Main(string[] args)
        {
            CoffeeDispenser coffeeDispenser = new CoffeeDispenser();
            bool shutdown = false;
            Initialize(coffeeDispenser);
            do
            {
                Menu(coffeeDispenser, ref shutdown);
            }
            while (!shutdown);
        }
        static void Initialize(CoffeeDispenser coffeeDispenser) {
            coffeeDispenser.Refill(100);
        }
        static void Menu(CoffeeDispenser coffeeDispenser, ref bool shutdown)
        {
            Console.WriteLine("Was möchten Sie tun?");
            Console.WriteLine("[1] Getränk kaufen");
            Console.WriteLine("[2] Kaffeeautomat verwalten");
            char choice = Console.ReadKey().KeyChar;
            Console.Clear();
            switch (choice)
            {
                case '1':
                    EBeverageType beverageType = SelectBeverage();
                    EMilk milk = SelectMilk();
                    ESugar sugar = SelectSugar();
                    BuyBeverage(coffeeDispenser, beverageType, milk, sugar);
                    break;

                case '2':
                    Administration(coffeeDispenser, ref shutdown);
                    break;

                default:
                    Console.WriteLine("Falsche Eingabe - probiere nochmal: ");
                    Menu(coffeeDispenser, ref shutdown);
                    break;
            }
       

        }

        static void BuyBeverage(CoffeeDispenser coffeeDispenser, EBeverageType type, EMilk milk, ESugar sugar)
        {
            Console.Clear();
            Beverage beverage;
            try
            {
                beverage = coffeeDispenser.ProduceBeverage(type, milk, sugar);
                Console.WriteLine("\n\t" + beverage.ToString() + coffeeDispenser.PayBeverage(beverage));
            }
            catch (Exception e)
            {
                Console.WriteLine("Fehler:" + e.Message + "\n" + "Befüllen Sie den Automaten bevor Sie ein Getränk bestellen\n");
            }
        }
        static void Administration(CoffeeDispenser coffeeDispenser, ref bool shutdown)
        {
            Console.Clear();
            Console.WriteLine("Was wollen Sie am Kaffeeautomat verwalten?");
            Console.WriteLine("[1] Geld abfragen");
            Console.WriteLine("[2] Kaffeeautomat auffüllen");
            Console.WriteLine("[3] Abschalten");

            char manageChoice = Console.ReadKey().KeyChar;
            Console.Clear();
            switch (manageChoice)
            {
                case '1':
                    Console.WriteLine("\n Im Kaffeeautomaten befinden sich " + coffeeDispenser.CurrentAccountBalance + "0 Euro");
                    Console.ReadKey();
                    break;

                case '2':
                    coffeeDispenser.Refill(10);
                    Console.WriteLine("\n Kaffeeautomat wurde aufgefüllt");
                    Console.ReadKey();
                    break;

                case '3':
                    shutdown = true;
                    break;

                default:
                    Console.WriteLine("\nFalsche Eingabe -probiere nochmal");
                    Administration(coffeeDispenser, ref shutdown);
                    break;

            }
        }

        static EBeverageType SelectBeverage()
        {
            Console.Clear();
            Console.WriteLine("Welches Getränk wollen Sie?");
            Console.WriteLine("[1] Kaffee");
            Console.WriteLine("[2] Tee");
            Console.WriteLine("[3] Heiße Dunkle Schokolade");
            Console.WriteLine("[4] Heiße Weiße Schokolade");
            Console.WriteLine("[5] Karmaell Late");
            char beverageChoice = Console.ReadKey().KeyChar;
            EBeverageType type;
         

            switch (beverageChoice)
            {
                case '1':
                    type = EBeverageType.COFFEE;
                    break;
                case '2':
                    type = EBeverageType.TEA;
                    break;
                case '3':
                    type = EBeverageType.HOT_CHOCOLATE;
                    break;
                case '4':
                    type = EBeverageType.WHITE_CHOCOLATE;
                    break;
                case '5':
                    type = EBeverageType.CARAMELL_LATE;
                    break;
                default:
                    type = SelectBeverage();
                    break;
            }
            return type;
        }

        static EMilk SelectMilk()
        {
            EMilk milk = EMilk.NONE;
            Console.Clear();
            Console.WriteLine("Wollen Sie Milch in Ihrem Getränk?");
            Console.WriteLine("[1] Nein, danke");
            Console.WriteLine("[2] Milch");
            Console.WriteLine("[3] Sojamilch");
            Console.WriteLine("[4] Laktosefreie Milch");

            char milkChoice = Console.ReadKey().KeyChar;
            //Console.Clear();
            switch (milkChoice)
            {
                case '1':
                    milk = EMilk.NONE;
                    break;

                case '2':
                    milk = EMilk.MILK;
                    break;

                case '3':
                    milk = EMilk.SOJA;
                    break;
                case '4':
                    milk = EMilk.LACTOSE_FREE_MILK;
                    break;

                default:
                    milk = SelectMilk();
                    break;
            }
            return milk;
        }

        static ESugar SelectSugar()
        {
            ESugar sugar;
            Console.Clear();
            Console.WriteLine("Wollen Sie Zucker in Ihrem Getränk?");
            Console.WriteLine("[1] Nein, danke");
            Console.WriteLine("[2] Zucker");
            Console.WriteLine("[3] Brauner Zucker");
            Console.WriteLine("[4] Kokosblüten Zucker");

            char sugarChoice = Console.ReadKey().KeyChar;
         
            switch (sugarChoice)
            {
                case '1':
                    sugar = ESugar.NONE;
                    break;
                case '2':
                    sugar = ESugar.WHITE_SUGAR;
                    break;
                case '3':
                    sugar = ESugar.BROWN_SUGAR;
                    break;
                case '4':
                    sugar = ESugar.COCONUT_SUGAR;
                    break;
                default:
                    sugar = SelectSugar();
                    break;
            }
            return sugar;
        }
    }
}
