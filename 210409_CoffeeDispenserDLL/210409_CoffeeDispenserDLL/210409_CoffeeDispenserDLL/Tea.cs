﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210409_CoffeeDispenserDLL
{
    class Tea : Beverage
    {
        public Tea(EMilk milk, ESugar sugar) : base(milk, sugar)
        {
            SetPrice(1);
            Type = EBeverageType.TEA;
        }

        public override string ToString()
        {
            StringBuilder text = new StringBuilder("Tee");
            text.Append(base.MilkSugarInfo());
            return text.ToString();
        }
    }
}
