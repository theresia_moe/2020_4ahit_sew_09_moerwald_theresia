﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210409_CoffeeDispenserDLL
{
    class CaramellLate : Beverage
    {
        public CaramellLate(EMilk milk, ESugar sugar) : base(milk, sugar)
        {
            SetPrice(3);
            Type = EBeverageType.CARAMELL_LATE;
        }

        public override string ToString()
        {
            StringBuilder text = new StringBuilder("Karamell Latte");
            text.Append(base.MilkSugarInfo());
            return text.ToString();
        }
    }
}
