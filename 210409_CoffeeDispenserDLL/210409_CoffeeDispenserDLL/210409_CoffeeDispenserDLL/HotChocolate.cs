﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210409_CoffeeDispenserDLL
{
    class HotChocolate : Beverage
    {
        public HotChocolate(EMilk milk, ESugar sugar) : base(milk, sugar)
        {
            SetPrice(2.5);
            Type = EBeverageType.HOT_CHOCOLATE;
        }

        public override string ToString()
        {
            StringBuilder text = new StringBuilder("Heiße Schokolade");

            switch (Milk)
            {
                case EMilk.NONE:
                    text.Append(" heiße Schokolade ohne Milch ist keine heiße Schokolade"); break;
                case EMilk.SOJA:
                    text.Append(" mit Sojamilch");
                    break;
                case EMilk.MILK:
                    text.Append(" normal");
                    break;
                case EMilk.LACTOSE_FREE_MILK:
                    text.Append(" laktosefrei");
                    break;
                default:
                    break;
            }
            return text.ToString();
        }
    }
}
