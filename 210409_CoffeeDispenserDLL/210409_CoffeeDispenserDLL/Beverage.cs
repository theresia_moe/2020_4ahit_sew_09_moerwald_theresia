﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210409_CoffeeDispenserDLL
{
    public class Beverage
    {
        public double Price { get; protected set; }
        public EMilk Milk { get; protected set; }
        public ESugar Sugar { get; protected set; }
        public EBeverageType Type { get; protected set; }

        protected Beverage(EMilk milk, ESugar sugar)
        {
            this.Milk = milk;
            this.Sugar = sugar;
        }
        protected void SetPrice(double price)
        {
            this.Price = price;
            switch (Milk)
            {
                case EMilk.NONE:
                    this.Price += 0; break;
                case EMilk.SOJA:
                    this.Price += 0.5;
                    break;
                case EMilk.MILK:
                    this.Price += 0.25;
                    break;
                case EMilk.LACTOSE_FREE_MILK:
                    this.Price += 0.75;
                    break;
                default:
                    break;
            }
            switch (Sugar)
            {
                case ESugar.NONE:
                    this.Price += 0;  break;
                case ESugar.BROWN_SUGAR:
                    this.Price += 0.5;  break;
                case ESugar.WHITE_SUGAR:
                    this.Price += 0.25;  break;
                case ESugar.COCONUT_SUGAR:
                    this.Price += 0.75; break;
                default:
                    break;
            }
          
        }

        public String GetPrice()
        {
            return String.Format(" kostet {0:0.00} Euro", Price);
        }

        protected string MilkSugarInfo()
        {
            StringBuilder text = new StringBuilder();

            switch (Milk)
            {
                case EMilk.NONE:
                    text.Append(" ohne Milch");
                    break;
                case EMilk.SOJA:
                    text.Append(" mit Sojamilch");
                    break;
                case EMilk.MILK:
                    text.Append(" mit Milch");
                    break;
                case EMilk.LACTOSE_FREE_MILK:
                    text.Append(" mit Laktosefreier Milch");
                    break;
                default:
                    break;
            }

            switch (Sugar)
            {
                case ESugar.NONE:
                    text.Append(" ohne Zucker");
                    break;
                case ESugar.BROWN_SUGAR:
                    text.Append(" mit braunem Zucker");
                    break;
                case ESugar.WHITE_SUGAR:
                    text.Append(" mit weißem Zucker");
                    break;
                case ESugar.COCONUT_SUGAR:
                    text.Append(" mit Kokosblüten Zucker");
                    break;
                default:
                    break;
            }


            return text.ToString();
        }
    }
}
