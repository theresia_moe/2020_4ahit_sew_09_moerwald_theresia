﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210409_CoffeeDispenserDLL
{
    public class CoffeeDispenser : ICoffeeDispenser
    {
        int cofffeeAmount;
        int teaAmount;
        int hotChocolateAmount;
        int whiteChocolateAmount;
        int caramellLatteAmount;
        int milkAmount;
        int sojaMilkAmount;
        int lactoseFreeAmount;
        int brownSugarAmount;
        int whiteSugarAmount;
        int coconutSugarAmount;        

        public double CurrentAccountBalance { get; private set; }

        public string PayBeverage(Beverage beverage) {
            CurrentAccountBalance += beverage.Price;
            return beverage.GetPrice() + ": Bezahlung abgeschlossen! \n";
        }

        public Beverage ProduceBeverage(EBeverageType type, EMilk milk, ESugar sugar)
        {
            Beverage b;
            switch (type)
            {
                case EBeverageType.COFFEE:
                    b = new Coffee(milk, sugar);
                    break;
                case EBeverageType.TEA:
                    b = new Tea(milk, sugar);
                    break;
                case EBeverageType.HOT_CHOCOLATE:
                    b = new HotChocolate(milk, sugar);
                    break;
                case EBeverageType.WHITE_CHOCOLATE:
                    b = new WhiteChocolate(milk, sugar);
                    break;
                case EBeverageType.CARAMELL_LATE:
                    b = new CaramellLate(milk, sugar);
                    break;
                default:
                    throw new Exception("Getränk nicht verfügbar");
            }
            UseSupplies(b);
            return b;
        }

        private void UseSupplies(Beverage b) {
            switch (b.Type)
            {
                case EBeverageType.COFFEE:
                    if (cofffeeAmount > 0)
                        cofffeeAmount--;
                    else
                        throw new Exception("Kaffe nachfüllen!");
                    break;
                case EBeverageType.TEA:
                    if (teaAmount > 0)
                        teaAmount--;
                    else
                        throw new Exception("Tee nachfüllen!");
                    break;                   
                case EBeverageType.HOT_CHOCOLATE:
                    if (hotChocolateAmount > 0)
                        hotChocolateAmount--;
                    else
                        throw new Exception("Heiße Schokolade nachfüllen!");
                    break;
                case EBeverageType.WHITE_CHOCOLATE:
                    if (whiteChocolateAmount > 0)
                        whiteChocolateAmount--;
                    else
                        throw new Exception("Weiße Schokolade nachfüllen!");
                    break;
                case EBeverageType.CARAMELL_LATE:
                    if (caramellLatteAmount > 0)
                        caramellLatteAmount--;
                    else
                        throw new Exception("Karamell Late nachfüllen!");
                    break;
                default:
                    break;
            }

            switch (b.Milk)
            {
                case EMilk.NONE: break;
                case EMilk.SOJA:
                    if (sojaMilkAmount > 0)
                        sojaMilkAmount--;
                    else
                        throw new Exception("Sojamilch nachfüllen!");
                    break;
                case EMilk.MILK:
                    if (milkAmount > 0)
                        milkAmount--;
                    else
                        throw new Exception("Milch nachfüllen!");
                    break;
                case EMilk.LACTOSE_FREE_MILK:
                    if (lactoseFreeAmount > 0)
                        lactoseFreeAmount--;
                    else
                        throw new Exception("Laktosefreie Milch nachfüllen!");
                    break;
                default:
                    break;
            }

            switch (b.Sugar)
            {
                case ESugar.NONE:
                    break;
                case ESugar.BROWN_SUGAR:
                    if (brownSugarAmount > 0)
                        brownSugarAmount--;
                    else
                        throw new Exception("Braunen Zucker nachfüllen!");
                    break;
                case ESugar.WHITE_SUGAR:
                    if (whiteSugarAmount > 0)
                        whiteSugarAmount--;
                    else
                        throw new Exception("Weißen Zucker nachfüllen!");
                    break;
                case ESugar.COCONUT_SUGAR:
                    if (coconutSugarAmount > 0)
                        coconutSugarAmount--;
                    else
                        throw new Exception("Kokosblütenzucker nachfüllen!");
                    break;
                default:
                    break;
            }

        }

        public string Refill(int amount) {
            cofffeeAmount = amount;
            teaAmount = amount;
            hotChocolateAmount = amount;
            whiteChocolateAmount = amount;
            caramellLatteAmount = amount;
            milkAmount = amount;
            sojaMilkAmount = amount;
            lactoseFreeAmount = amount;
            brownSugarAmount = amount;
            whiteSugarAmount = amount;
            coconutSugarAmount = amount;
            return $"Kaffeeautomat wurde auf {amount} gefüllt!";
        }
    }
}
