﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210409_CoffeeDispenserDLL
{

    public enum EBeverageType { COFFEE, TEA, HOT_CHOCOLATE, WHITE_CHOCOLATE, CARAMELL_LATE};
    public enum EMilk { NONE, SOJA, MILK, LACTOSE_FREE_MILK };
    public enum ESugar { NONE, BROWN_SUGAR, WHITE_SUGAR, COCONUT_SUGAR }
    public interface ICoffeeDispenser
    {
        double CurrentAccountBalance { get; }
        string Refill(int amount);
        Beverage ProduceBeverage(EBeverageType type, EMilk milk, ESugar sugar);
        string PayBeverage(Beverage beverage);
    }
}
