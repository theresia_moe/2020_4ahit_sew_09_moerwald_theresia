﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sync_with_Mutex2
{
    class Program
    {
        //Synchronization with Mutex2

        private static Mutex mut = new Mutex();
        private const int numIterations = 1;
        private const int numThreads = 3;

        static void Main(string[] args)
        {
            for (int i = 0; i < numThreads; i++)
            {
                Thread newThread = new Thread(new ThreadStart(ThreadProc));
                newThread.Name = String.Format("Thread{0}", i + 1);
                newThread.Start();
            }
            Console.ReadLine();
        }
          

        private static void ThreadProc()
        {
            for (int i = 0; i < numIterations; i++)
            {
                UseResource();
            }
        }

        private static void UseResource()
        {
            Console.WriteLine("{0} is requesting the mutex", Thread.CurrentThread.Name);

            if (mut.WaitOne())
            {
                Console.WriteLine("{0} has enterred the protected area", Thread.CurrentThread.Name);

                Thread.Sleep(500);

                Console.WriteLine("{0} is leaving the protexted area", Thread.CurrentThread.Name);

                mut.ReleaseMutex();

                Console.WriteLine("{0} has released the mutex", Thread.CurrentThread.Name);
            }
            else
            {
                Console.WriteLine("{0} will not axquire the mutex", Thread.CurrentThread.Name);
            }
        }
    }
}
