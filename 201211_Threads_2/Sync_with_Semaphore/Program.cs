﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sync_with_Semaphore
{
    class Program
    {
        //Synchronization with Semaphore
        static void Main(string[] args)
        {
            for(int i = 0; i < 10; i++)
            {
                Thread threadObject = new Thread(new ThreadStart(PerformSomeWork));
                threadObject.Name = "Thread Name: " + i;
                threadObject.Start();
            }
        }

        public static Semaphore threadPool = new Semaphore(3, 5);

        private static void PerformSomeWork()
        {
            threadPool.WaitOne();
            Console.WriteLine("Thread {0} is inside the crutical section...", Thread.CurrentThread.Name);
            Thread.Sleep(10000);
            threadPool.Release();
        }

    }
}
