﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _201015_WPFStudentSubject
{
    class AddStudentCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        public StudentViewModel parent;
        public AddStudentCommand(StudentViewModel vm)
        {
            parent = vm;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            parent.AddStudent();
        }
    }
}
