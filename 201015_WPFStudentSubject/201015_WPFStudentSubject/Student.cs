﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201015_WPFStudentSubject
{
    class Student
    {
        public Student(string firstName, string lastName, int age, EClass clazz)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            Clazz = clazz;
        }
        public Student() { }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; } = 0;
        public EClass Clazz { get; set; }

        public string ToCSV()
        {
            return FirstName + ";" + LastName + ";" + Age + ";" + Clazz.ToString();
        }
    }
}
