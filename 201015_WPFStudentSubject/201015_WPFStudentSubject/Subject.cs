﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201015_WPFStudentSubject
{
    class Subject
    {
        public Subject(string code, string name, EDifficulty difficulty, bool favSubject)
        {
            Code = code;
            Name = name;
            Difficulty = difficulty;
            FavSubject = favSubject;
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public EDifficulty Difficulty { get; set; }
        public bool FavSubject { get; set; }

        public string ToCSV()
        {
            return Code + ";" + Name + ";" + Difficulty.ToString() + ";" + FavSubject;
        }
    }
}
