﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;

namespace _201015_WPFStudentSubject
{
    class LoadCommandSubj : ICommand
    {
        public event EventHandler CanExecuteChanged;
        SubjectViewModel parent;
        public LoadCommandSubj(SubjectViewModel vm)
        {
            parent = vm;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            parent.ReadCSV();
        }
    }
}
