﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;


namespace _201015_WPFStudentSubject
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    enum EClass { IT1, IT2, IT3, IT4, IT5 }
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            cb_Class.ItemsSource = Enum.GetValues(typeof(EClass)).Cast<EClass>();

        }

        private void txt_Age_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
