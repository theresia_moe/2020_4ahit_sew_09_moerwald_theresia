﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;

namespace _201015_WPFStudentSubject
{
    class StudentViewModel : INotifyPropertyChanged
    {
        ObservableCollection<Student> _students;
        public ObservableCollection<Student> Students
        {
            get
            {
                return _students;
            }
            set
            {
                _students = value;
                PropertyChange("Students");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand AddCommand { get; private set; }
        public ICommand LoadCommand { get; private set; }
        public ICommand AddStudentCommand { get; private set; }
        string path = "Students.csv";
        public StudentViewModel()
        {
            _students = new ObservableCollection<Student>();
            LoadCommand = new LoadCommand(this);
            AddCommand = new AddCommand(this);
            AddStudentCommand = new AddStudentCommand(this);

        }
        string firstname;
        public string CurrFirstname
        {
            get
            {
                return firstname;
            }
            set
            {
                firstname = value;
                PropertyChange("CurrFirstname");
            }

        }
        string lastname;
        public string CurrLastname
        {
            get
            {
                return lastname;
            }
            set
            {
                lastname = value;
                PropertyChange("CurrLastname");
            }
        }

        int age;
        public int CurrAge
        {
            get { return age; }
            set
            {
                age = value;
                PropertyChange("CurrAge");
            }
        }
        EClass clazz;
        public EClass CurrClazz
        {
            get
            {
                return clazz;
            }
            set
            {
                clazz = value;
                PropertyChange("CurrClazz");
            }
        }
        public void PropertyChange(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
        public void LoadfromCSV()
        {

            Students = new ObservableCollection<Student>();
            StreamReader sr = new StreamReader(path);
            using (sr)
            {
                if (File.Exists(path))
                {
                    while (!sr.EndOfStream)
                    {
                        string stud = sr.ReadLine();
                        string[] splstud = stud.Split(';');
                        Students.Add(new Student(splstud[0], splstud[1], Convert.ToInt32(splstud[2]), (EClass)Enum.Parse(typeof(EClass), splstud[3])));
                    }
                }

            }
        }
        public void SaveInCSV()
        {

            StreamWriter wr = new StreamWriter(path);
            using (wr)
            {
                for (int i = 0; i < Students.Count; i++)
                {
                    wr.WriteLine(Students[i].ToCSV());
                }
            }

        }
        public void AddStudent()
        {

            Students.Add(new Student(firstname, lastname, age, clazz));
        }
    }
}
