﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.IO;

namespace _201015_WPFStudentSubject
{
    enum EDifficulty { EASY, MIDDLE, HARD }
    class SubjectViewModel : INotifyPropertyChanged
    {
        ObservableCollection<Subject> _subjects;
        string path = "subjects.csv";
        public ObservableCollection<Subject> Subjects
        {
            get
            {

                return _subjects;
            }
            set
            {
                _subjects = value;
                OnPropertyChange("Subjects");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand SaveCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand LoadCommand { get; set; }
        public ICommand AddSubjectCommand { get; set; }


        public SubjectViewModel()
        {
            _subjects = new ObservableCollection<Subject>();
            SaveCommand = new SaveCommand(this);
            DeleteCommand = new DeleteCommand(this);
            LoadCommand = new LoadCommandSubj(this);
            AddSubjectCommand = new AddSubjectCommand(this);
        }
        string code;
        public string CurrCode
        {
            get { return code; }
            set
            {
                code = value;
                OnPropertyChange("CurrCode");
            }
        }
        string name;
        public string CurrName
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChange("CurrName");
            }
        }
        EDifficulty difficulty;

        bool curreasy;
        bool currhard;
        bool currmiddle;
        public bool CurrEasy
        {
            get
            {
                return curreasy;
            }
            set
            {
                curreasy = value;
                if (curreasy)
                {
                    difficulty = EDifficulty.EASY;
                }
                OnPropertyChange("CurrEasy");
            }
        }
        public bool CurrMiddle
        {
            get
            {
                return currmiddle;
            }
            set
            {
                currmiddle = value;
                if (currmiddle)
                {
                    difficulty = EDifficulty.MIDDLE;
                }
                OnPropertyChange("CurrMiddle");
            }
        }

        public bool CurrHard
        {
            get
            {
                return currhard;
            }
            set
            {
                currhard = value;
                if (currhard)
                {
                    difficulty = EDifficulty.HARD;
                }
                OnPropertyChange("CurrHard");
            }
        }
        bool favSubject;
        public bool CurrFavSubject
        {
            get
            {
                return favSubject;
            }
            set
            {
                favSubject = value;
                OnPropertyChange("CurrFavSubject");
            }
        }
        int itemCount;
        public int ItemCount
        {
            get
            {
                return Subjects.Count;

            }
            set
            {
                itemCount = value;
            }
        }

        void OnPropertyChange(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
        public void SavetoCSV()
        {
            StreamWriter wr = new StreamWriter(path);
            using (wr)
            {
                for (int i = 0; i < Subjects.Count; i++)
                {
                    wr.WriteLine(Subjects[i].ToCSV());
                }
            }
        }
        public void ReadCSV()
        {
            Subjects = new ObservableCollection<Subject>();
            StreamReader sr = new StreamReader(path);
            using (sr)
            {
                if (File.Exists(path))
                {
                    while (!sr.EndOfStream)
                    {
                        string sub = sr.ReadLine();
                        string[] splsub = sub.Split(';');
                        Subjects.Add(new Subject(splsub[0], splsub[1], (EDifficulty)Enum.Parse(typeof(EDifficulty), splsub[2]), Convert.ToBoolean(splsub[3])));
                        OnPropertyChange("ItemCount");
                    }
                }

            }
        }
        public void Delete()
        {
            for (int i = 0; i < Subjects.Count; i++)
            {
                if (Subjects[i].FavSubject)
                {
                    Subjects.Remove(Subjects[i]);

                }
            }
            OnPropertyChange("ItemCount");

        }
        public void AddSubject()
        {

            Subjects.Add(new Subject(CurrCode, CurrName, difficulty, CurrFavSubject));
            OnPropertyChange("ItemCount");
        }
    }
}
