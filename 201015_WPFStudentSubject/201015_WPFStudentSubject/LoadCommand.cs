﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _201015_WPFStudentSubject
{
    class LoadCommand : ICommand
    {
        StudentViewModel parent;
        public event EventHandler CanExecuteChanged;

        public LoadCommand(StudentViewModel vm)
        {
            parent = vm;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            parent.LoadfromCSV();
        }
    }
}
