﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Linq_Student;

namespace Linq_Student.Tests
{
    [TestClass()]
    public class LinqExtensionMethodsTests
    {
        [TestMethod()]
        public void SortNamesTest()
        {
            string[] names = NameGenerator.GetNames();
            //Test SortNames Method
            string[] sorted = LinqExtensionMethods.SortNames(names);

            for (int i = 0; i < sorted.Length - 1; i++)
            {
                Assert.AreEqual(sorted[i].CompareTo(sorted[i + 1]), -1);
            }
            //Assert.Fail("Unit Test is missing");
        }

        [TestMethod()]
        public void ConcatNamesTest()
        {
            string[] names = NameGenerator.GetNames();
            string[] pupils = NameGenerator.GetPupils();

            //Test ConcatNames Method
            string[] result = LinqExtensionMethods.ConcatNames(names, pupils);

            //Use the Length of the collections: names+pupils == result 
            //Assert.Fail("Unit Test is missing");
            Assert.AreEqual(names.Length + pupils.Length, result.Length);
        }

        [TestMethod()]
        public void RemoveRedundantTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            string[] pupils = LinqExtensionMethods.SortNames(NameGenerator.GetPupils());
            string[] concat = LinqExtensionMethods.ConcatNames(names, pupils); ;

            //Test remove redundant
            List<string> result = LinqExtensionMethods.RemoveRedundant(concat).ToList();

            //if you take out element by element - every one should be unique
            //so there should not be a second one with the same value in the list
            while (result.Count > 1)
            {
                string removedname = result[0];
                result.RemoveAt(0);
                foreach (string name in result)
                {
                    if (name == removedname)
                        Assert.Fail("Redundat values are existing");
                }
                // Assert.Fail("Redundat values are existing");
            }
        }

        [TestMethod()]
        public void UnionNamesTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            string[] pupils = LinqExtensionMethods.SortNames(NameGenerator.GetPupils());

            //Use Concat & Remove 
            string[] concat = LinqExtensionMethods.ConcatNames(names, pupils);
            string[] result = LinqExtensionMethods.RemoveRedundant(concat);

            //Test UnionNames 
            string[] union = LinqExtensionMethods.UnionNames(names, pupils);

            Assert.AreEqual(result.Length, union.Length);
            //Both collections should have the same amount of elements (result == union)
            // Assert.Fail("Unit Test is missing");
        }

        [TestMethod()]
        public void ReverseNamesTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            //Test ReverseName Method
            string[] result = LinqExtensionMethods.ReverseNames(names);
            Assert.AreEqual(names[0], result[result.Length - 1]);
            //First element should now be the last element
            //Assert.Fail("Unit Test is missing");
        }
    }

    [TestClass()]
    public class LinqPetMethodsTests
    {
        Pet[] cats = Pet.GetCats();
        Pet[] dogs = Pet.GetDogs();
       


        //TODO: Test your own PetMethods
        [TestMethod]
        public void ConcatPets()

        {
                     IEnumerable<string> query = LinqExtensionMethods.PetConcaterClass.ConcatArrays(cats, dogs);
            Assert.AreEqual(cats.Length + dogs.Length, query.Count());

        }

        [TestMethod]
        public void SortPets()
        {
            Pet[] pets = new Pet[cats.Length + dogs.Length];
            cats.CopyTo(pets, 0);
            dogs.CopyTo(pets, cats.Length);

            IEnumerable<Pet> sorted = LinqExtensionMethods.PetConcaterClass.SortNames(pets);

            for (int i = 0; i < sorted.Count() - 1; i++)
            {
                Assert.AreEqual(sorted.ElementAt(i).Name.CompareTo(sorted.ElementAt(i+1).Name), -1);
            }
        }

        [TestMethod]
        public void UnionPets()
        {

            IEnumerable<Pet> unionpets = LinqExtensionMethods.PetConcaterClass.UnionPets(cats, dogs);

            Assert.AreEqual(unionpets.Count(), cats.Length + dogs.Length);

        }

        [TestMethod]
        public void IntersectPets()
        {
            IEnumerable<Pet> query = LinqExtensionMethods.PetConcaterClass.IntersectPets(cats, dogs);
            foreach (Pet pet in query)
            {
                bool iscat = false;
                bool isdog = false;
                foreach (Pet cat in cats)
                {
                    if (cat.Name == pet.Name)
                        iscat = true;
                }
                foreach (Pet dog in dogs)
                {
                    if (dog.Name == pet.Name)
                        isdog = true;
                }
                Assert.IsTrue(iscat && isdog);
            }
        }
        [TestMethod]
        public void ExceptPets()
        {
            IEnumerable<Pet> query = LinqExtensionMethods.PetConcaterClass.ExpectPets(cats, dogs);
            foreach (Pet pet in query)
            {
                bool iscat = false;
                bool isdog = false;
                foreach (Pet cat in cats)
                {
                    if (cat.Name == pet.Name)
                        iscat = true;
                }
                foreach (Pet dog in dogs)
                {
                    if (dog.Name == pet.Name)
                        isdog = true;
                }
                Assert.IsTrue(iscat ^ isdog);
            }
        }

        [TestMethod]
        public void AverageAge()
        {
            double sum = 0;
            double average;
            Pet[] pets = new Pet[cats.Length + dogs.Length];
            cats.CopyTo(pets, 0);
            dogs.CopyTo(pets, cats.Length);

            double averageLinq = LinqExtensionMethods.PetConcaterClass.AverageAge(pets);

            foreach(Pet p in pets)
            {
                sum += p.Age;
            }

            average = sum / pets.Length;

            Assert.AreEqual(average, averageLinq);

        }
    }
}
