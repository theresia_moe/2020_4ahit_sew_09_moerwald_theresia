﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq_Student
{

    public class Program {
        #region Ex2_LinqQueryExamples 
        public static void TestProjection_Select() {
            //Change Methods 
            //Add return-values and parameterise them
            Ex2_LinqQueryExamples.SelectRoundedValues();
            Ex2_LinqQueryExamples.SelectAnonymousType();
            Ex2_LinqQueryExamples.SelectWithIndex();
            Ex2_LinqQueryExamples.SelectManyForCrossJoin();
        }
        public static void TestOrderBy_Score() {
            // Data source.
            int[] scores = { 90, 71, 82, 93, 75, 82, 100, 39, 99 };
            int greaterThan = 80;
            // Execute the query to produce the results
            int[] result = Ex2_LinqQueryExamples.FSelectScoresGreater(scores, greaterThan);
            foreach (int testScore in result) {
                Console.Write("{0}, ", testScore);
            }
            Console.WriteLine();
            //Output: 100, 99, 93, 90, 82, 82,
        }
        public static void TestOrderByDesc_Strings() {
            string[] words = { "Affenbrotbaum", "Chiasamen",
                       "Acai", "Matcha", "Flohsamenschalen" };
            Console.WriteLine("\nSort Words Alphabetically");
            string[] names = Ex2_LinqQueryExamples.MSortWords(words);
            foreach (string str in names)
                Console.WriteLine(str);
            Console.WriteLine("\nSort By Length");
            string[] restult = Ex2_LinqQueryExamples.MSortByWordLengthDesc(words);
            foreach (string str in restult)
                Console.WriteLine(str);
        }
        public static void TestOrderByThenBy() {
            string[] words = { "grape", "passionfruit", "banana", "mango",
                      "orange", "raspberry", "apple", "blueberry" };
            string[] orderd1 = Ex2_LinqQueryExamples.FOrderByLenghtThenByAlphabet(words);
            Console.WriteLine("Free Notation: Order by wordlenght then by names");
            foreach (string fruit in orderd1)
                Console.WriteLine(fruit);
            Console.WriteLine();
            string[] orderd2 = Ex2_LinqQueryExamples.FOrderByLenghtThenByAlphabet(words);
            Console.WriteLine("Method Notation: Order by wordlenght then by names");
            foreach (string fruit in orderd2)
                Console.WriteLine(fruit);
        }
        public static void TestOrderByWithCars() {
            Car[] cars = Car.GenerateCars();
            Console.WriteLine("Unsorted list of cars:");
            foreach (Car car in cars)
                Console.WriteLine(String.Format("{0}: {1} horses", car.Name, car.HorsePower));

            Car[] result1 = Ex2_LinqQueryExamples.CarOrderByExample.MOrderCarByHorsepower(cars);
            Console.WriteLine("\nMethod Syntas: \nList of cars sorted by horsepower:");
            foreach (Car car in result1)
                Console.WriteLine(String.Format("{0}: {1} horses", car.Name, car.HorsePower));

            Car[] result2 = Ex2_LinqQueryExamples.CarOrderByExample.FOrderCarByHorsepower(cars);
            Console.WriteLine("\nQuery Syntas: \nList of cars sorted by horsepower:");
            foreach (Car car in result2)
                Console.WriteLine(String.Format("{0}: {1} horses", car.Name, car.HorsePower));

        }
        #endregion

        #region Ex1_LinqExtentionMethods
        public static void TestEasyExtensionMethodExamples() {
            string[] names = NameGenerator.GetNames();
            PrintList(names, "All Names:");
            string[] pupils = NameGenerator.GetPupils();
            string[] concats = LinqExtensionMethods.ConcatNames(names, pupils);
            PrintList(concats, "Concat Names");
            string[] noredu = LinqExtensionMethods.RemoveRedundant(concats);
            PrintList(noredu, "Remove Doubles");
            string[] sorted = LinqExtensionMethods.SortNames(noredu);
            PrintList(sorted, "Sorted Names");
            string[] union = LinqExtensionMethods.UnionNames(names, pupils);
            PrintList(union, "United Names & Pupils");
            string[] reverse = LinqExtensionMethods
                .ReverseNames(LinqExtensionMethods.SortNames(union));
            PrintList(union, "United & Sorted Descending");
        }
        public static void TestPetExamples() {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            IEnumerable<string> query = LinqExtensionMethods.
                    PetConcaterClass.ConcatArrays(cats, dogs);
            Console.WriteLine("Concat Pets into a collection of Names:");
            foreach (var e in query)
                Console.Write("{0}, ", e);
            Console.WriteLine("\n");
        }
        #endregion

        #region Ex3_LinqGrouping
        static void TestLinqGrouping() {
            Ex3_LinqGroupingExample.TestLinqBsp();
        }
        #endregion

        #region Main
        public static void PrintList(string[] result, string title) {
            Console.WriteLine("\n ------------- {0} ------------- \n", title);
            for (int i = 0; i < result.Length; i++) {
                Console.Write(result[i] + ", ");
                if ((i + 1) % 5 == 0)
                    Console.WriteLine("\n");
            }
            Console.WriteLine("\n");
        }
        public static void Main(String[] args) {
            Console.WriteLine();
            ////Test Ex1_LinqExtMethExample
            Console.WriteLine("Test Ex1_LinqExtMethExample");
            TestEasyExtensionMethodExamples();
            //TestPetExamples();

            ////Test Ex2_LinqQueryExample
            Console.WriteLine("Test Ex2_LinqQueryExample");
            //TestProjection_Select();
            //TestOrderBy_Score();
            //TestOrderByDesc_Strings();
            //TestOrderByThenBy();
            //TestOrderByWithCars();

            ////Test Ex3_LinqGroupingExamples
            Console.WriteLine("Test Ex3_LinqGroupingExamples");
            Console.WriteLine("Tests ausgeführt!");
        }
        #endregion
    }


}
