﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq_Student
{
    public class NameGenerator
    {
        static string[] names = { "Benedikt", "Raphael",
            "Sonja", "Simon", "Alexa", "Alex", "Benjamin" };
        static string[] pupils = { "Nora", "Simon", "Markus", "Christoph",
            "Josef", "Benjamin", "Daniel", "Benedikt", "Peter", "Gregor",
            "Dominik", "Simon", "Peter", "Alexander", "Daniel", "Christina", "Raphael"};
        public static string[] GetNames()
        {
            return names;
        }
        public static string[] GetPupils()
        {
            return pupils;
        }
    }
    public class Pet
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public static Pet[] GetCats()
        {
            Pet[] cats = { new Pet { Name="Barley", Age=8 },
                       new Pet { Name="Boots", Age=4 },
                       new Pet { Name="Whiskers", Age=1 } };
            return cats;
        }
        public static Pet[] GetDogs()
        {
            Pet[] dogs = { new Pet { Name="Bounder", Age=3 },
                        new Pet { Name="Snoopy", Age=14 },
                        new Pet { Name="Fido", Age=9 } };
            return dogs;
        }
    }
    public class LinqExtensionMethods
    {
        #region Easy Extension Methods
        public static string[] SortNames(string[] names)
        {

            IEnumerable<string> result = names.OrderBy(s => s);
            return result.ToArray();
        }

        public static string[] ConcatNames(string[] names, string[] pupils)
        {
            return names.Concat(pupils).ToArray();
        }

        public static string[] RemoveRedundant(string[] names)
        {
            return names.Distinct().ToArray();
        }

        public static string[] UnionNames(string[] names, string[] pupils)
        {
            return pupils.Union(names).ToArray();
        }

        public static string[] ReverseNames(string[] names)
        {
            return names.Reverse().ToArray();
        }
        #endregion

        #region Concat_PetArrays
       
        public class PetConcaterClass
        {
            public static IEnumerable<string> ConcatArrays(Pet[] cats, Pet[] dogs)
            {
                IEnumerable<string> query = cats.Select(cat => cat.Name).Concat(dogs.Select(dog => dog.Name));
                return query;
            }
            
            public static IEnumerable<Pet> SortNames(Pet[] pets)
            {
                return pets.OrderBy(s => s.Name);
            }

            public static IEnumerable<Pet> UnionPets(Pet[] cats, Pet[] dogs)
            {
                return cats.Union(dogs).ToArray();
            }

            public static IEnumerable<Pet> IntersectPets(Pet[] cats, Pet[] dogs)
            {
                return cats.Intersect(dogs).ToArray();
            }

            public static IEnumerable<Pet> ExpectPets(Pet[] cats, Pet[] dogs)
            {
                return cats.Except(dogs).ToArray();
            }

            public static double AverageAge(Pet[] pet)
            {
                return pet.Average(p=>p.Age);
            }

            
        }
        //TODO
        //Add Union, Intersect, Except Queries and some Aggregation Operations
        #endregion

    }
}
