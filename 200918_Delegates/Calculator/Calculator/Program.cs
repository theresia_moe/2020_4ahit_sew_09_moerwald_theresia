﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        
        public delegate double NumberChanger(double i1, double i2);  
        static void Main(string[] args)
        {
            NumberChanger op;
            Console.WriteLine("Zahl 1: ");
            double x = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Zahl 2: ");
            double y = Convert.ToDouble(Console.ReadLine());            
            Console.WriteLine(" +(1) -(2) *(3) /(4)");
            int opVal = Convert.ToInt32(Console.ReadLine());

            switch (opVal)
            {
                case 1:
                    op = Add; break;
                case 2:
                    op = Sub; break;
                case 3:
                    op = Multi; break;
                case 4:
                    op = Div; break;
                default:
                    op = Add; break;
            }
            Console.WriteLine("Ergebniss: " + op(x, y));

            Console.ReadLine();


        }

        static double Add(double n1, double n2)
        {
            return n1 + n2;
        }
        static double Sub(double n1, double n2)
        {
            return n1 - n2;
        }
        static double Multi(double n1, double n2)
        {
            return n1 * n2;
        }
        static double Div(double n1, double n2)
        {
            return n1 / n2;
        }
    }
}
