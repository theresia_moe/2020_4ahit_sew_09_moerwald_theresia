﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee
{
    class Company
    {
        public delegate void CustomDel(Employee employee);
        CustomDel SalaryRaisDel, PositionDateDel, EmployeePromoteMulticastDel;
        public List<Employee> Employees { get; }
        public Company()
        {
            Employees = new List<Employee>()
            {
                new Employee(){Id = 1, Name = "Max", Salary = 10},
                new Employee(){Id = 2, Name = "John", Salary = 20},
                new Employee(){Id = 3, Name = "Kurt", Salary = 15},
                new Employee(){Id = 4, Name = "Nick", Salary = 25},
            };
            SalaryRaisDel = new CustomDel(EmployeeSalaryRaise);
            PositionDateDel = new CustomDel(PositionDateUpdate);
            EmployeePromoteMulticastDel = SalaryRaisDel + PositionDateDel;
        }
        public void PromoteEmployees()
        {
            foreach (Employee employee in Employees)
            {
                EmployeePromoteMulticastDel(employee);

                Console.WriteLine($"{employee.Id} - {employee.Name}");
                Console.WriteLine($" LastUpdate {employee.PositionChangeDate.ToShortDateString()}");
                Console.WriteLine($"Salary = {employee.Salary}");
            }
        }
        private void PositionDateUpdate(Employee employee)
        {
            employee.PositionChangeDate = DateTime.Now;
        }
        private void EmployeeSalaryRaise(Employee employee)
        {
            employee.Salary += employee.Salary * (decimal)0.1;
        }
    }
}
