﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace _201015_YesNoConverter
{
    class YesNoBooleanConverterVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private YesNoBooleanConverter converter = new YesNoBooleanConverter();

        private bool check = true;
        private string input = "no";



        public bool Check
        {
            get { return check; }
            set
            {
                check = value;
                input = (string)converter.ConvertBack(check);
                CallPropertyChanged();
                CallPropertyChanged("Input");
            }
        }

        public string Input
        {
            get { return input; }
            set
            {
                input = value;
                check = (bool)converter.Convert(input);
                CallPropertyChanged();
                CallPropertyChanged("Check");
            }
        }




        protected void CallPropertyChanged([CallerMemberName] string property = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
