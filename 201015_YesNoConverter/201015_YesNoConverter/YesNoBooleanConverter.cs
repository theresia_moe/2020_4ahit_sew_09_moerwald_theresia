﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace _201015_YesNoConverter
{
    class YesNoBooleanConverter : IValueConverter
    {
        public Object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            return Convert(value);
        }

        public Object Convert(object value)
        {
            switch (value.ToString().ToLower())
            {
                case "yes":
                case "oui":
                    return true;

                case "no":
                case "non":
                    return false;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            return ConvertBack(value);
        }

        public object ConvertBack(object value)
        {
            if (value is bool)
            {
                if ((bool)value == true)
                    return "yes";
                else
                    return "no";

            }
            return "no";
        }
    }
}
