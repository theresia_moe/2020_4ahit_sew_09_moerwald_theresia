﻿using System;
using System.Collections.Generic;
using System.Text;
using _210423_CoffeeDispenserDecorator_DLL;
using System.Windows;
using System.Windows.Input;

namespace _210423_CoffeeDispenserDecorator
{
    class RelayCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        readonly Func<bool> _canExecute;
        readonly Action _execute;

        public RelayCommand(Action execute) : this(execute, null)
        {

        }
        public RelayCommand(Action execute, Func<bool> canExecute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException("execute");
            }
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute();
        }

        public void Execute(object parameter)
        {
            try
            {
                _execute();
            }
            catch (CoffeeDispenserOutOfFuelException e)
            {
                MessageBox.Show(e.Message);
            }
            catch (NullReferenceException e)
            {
                MessageBox.Show("Wählen Sie als erstes ein Getränk aus bevor sie Zutaten hinzufügen");
            }
        }
    }
}
