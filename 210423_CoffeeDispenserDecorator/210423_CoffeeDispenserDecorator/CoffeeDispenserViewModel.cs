﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows;
using _210423_CoffeeDispenserDecorator_DLL;
using System.Windows.Threading;

namespace _210423_CoffeeDispenserDecorator
{
    class CoffeeDispenserViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChange([CallerMemberName] string property = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        private CoffeeDispenser CoffeeDispenser { get; set; }
        private IBeverage Beverage { get; set; }
        public CoffeeDispenserViewModel()
        {
            CoffeeDispenser = new CoffeeDispenser();
            UpdateOrderAndPrice();
        }
       
        private string order;
        public string Order
        {
            get { return order; }
            set
            {
                order = value;
                OnPropertyChange();
            }
        }
        private double price;
        public double Price
        {
            get { return price; }
            set
            {
                price = value;
                OnPropertyChange();
            }
        }
        private double currentAccountBalance;
        public double CurrentAccountBalance
        {
            get { return currentAccountBalance; }
            set
            {
                currentAccountBalance = value;
                OnPropertyChange();
            }
        }
        private void UpdateOrderAndPrice()
        {
            if (Beverage != null)
            {
                Order = Beverage.GetDescription();
                Price = Beverage.GetPrice();
            }
            else
            {
                Order = "";
                Price = 0;
            }
        }
        //CoffeeDispenserController:
        public RelayCommand OrderAndPay => new RelayCommand(() => {
            try
            {
                CoffeeDispenser.CurAccountBalance += Beverage.GetPrice();
            }
            catch (NullReferenceException e)
            {
                MessageBox.Show("Please choose a beverage");
                return;
            }
            CurrentAccountBalance = CoffeeDispenser.CurAccountBalance;
            Beverage = null;
            UpdateOrderAndPrice();
        });

        public RelayCommand FillCoffeeDispenser => new RelayCommand(() => {
            CoffeeDispenser.Refill(50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50);
        });

        public RelayCommand GetTea => new RelayCommand(() => { Beverage = CoffeeDispenser.GetTea(); UpdateOrderAndPrice(); });
        public RelayCommand GetHotChocolate => new RelayCommand(() => { Beverage = CoffeeDispenser.GetHotChocolate(); UpdateOrderAndPrice(); });
        public RelayCommand GetCoffee => new RelayCommand(() => { Beverage = CoffeeDispenser.GetCoffee(); UpdateOrderAndPrice(); });
        public RelayCommand GetWhiteChocolate => new RelayCommand(() => { Beverage = CoffeeDispenser.GetWhiteChocolate(); UpdateOrderAndPrice(); });
        public RelayCommand GetCaramelLate => new RelayCommand(() => { Beverage = CoffeeDispenser.GetCaramelLate(); UpdateOrderAndPrice(); });
        public RelayCommand AddMilk => new RelayCommand(() => { Beverage = CoffeeDispenser.AddMilk(Beverage); UpdateOrderAndPrice(); });
        public RelayCommand AddSojaMilk => new RelayCommand(() => { Beverage = CoffeeDispenser.AddSojaMilk(Beverage); UpdateOrderAndPrice(); });
        public RelayCommand AddOatMilk => new RelayCommand(() => { Beverage = CoffeeDispenser.AddOatMilk(Beverage); UpdateOrderAndPrice(); });
        public RelayCommand AddLactosefreeMilk => new RelayCommand(() => { Beverage = CoffeeDispenser.AddLactosefreeMilk(Beverage); UpdateOrderAndPrice(); });
        public RelayCommand AddSugar => new RelayCommand(() => { Beverage = CoffeeDispenser.AddSugar(Beverage); UpdateOrderAndPrice(); });
        public RelayCommand AddBrownSugar => new RelayCommand(() => { Beverage = CoffeeDispenser.AddBrownSugar(Beverage); UpdateOrderAndPrice(); });
        public RelayCommand AddXylit => new RelayCommand(() => { Beverage = CoffeeDispenser.AddXylit(Beverage); UpdateOrderAndPrice(); });
        public RelayCommand AddStevia => new RelayCommand(() => { Beverage = CoffeeDispenser.AddStevia(Beverage); UpdateOrderAndPrice(); });
    }
}
