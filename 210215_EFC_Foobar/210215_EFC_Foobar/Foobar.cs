﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210215_EFC_Foobar
{
   public class Foobar
    {
        public int Id { get; set; }
        public Foo OneFoo { get; set; }
        public List<Bar> ListBar { get; set; } = new List<Bar>();
    }
}
