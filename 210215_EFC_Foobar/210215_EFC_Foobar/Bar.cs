﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace _210215_EFC_Foobar
{
   public class Bar
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

   //public class FirstBar : Bar { }
   //public class SecondBar : Bar { }
   //public class ThirdBar : Bar { }
}
