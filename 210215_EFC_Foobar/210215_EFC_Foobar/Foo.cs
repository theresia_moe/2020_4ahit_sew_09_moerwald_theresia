﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace _210215_EFC_Foobar
{
   public class Foo
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

   // [Table ("FirstFoo")]
   //public class FirstFoo : Foo
   // {
   //     public string First { get; set; }
   //     public override string ToString()
   //     {
   //         return $"{Name} - {First}";
   //     }
   // }

   // [Table("SecondFoo")]
   // public class SecondFoo : Foo
   // {
   //     public string Second { get; set; }
   //     public override string ToString()
   //     {
   //         return $"{Name} - {Second}";
   //     }
   // }

   // [Table("ThirdFoo")]

   //public class ThirdFoo : Foo
   // {
   //     public string Third { get; set; }
   //     public override string ToString()
   //     {
   //         return $"{Name} - {Third}";
   //     }
   // }





}
