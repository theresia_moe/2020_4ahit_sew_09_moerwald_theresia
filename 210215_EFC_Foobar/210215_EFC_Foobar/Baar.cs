﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210215_EFC_Foobar
{
   public class Baar
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Fooo> Fooos { get; set; } = new List<Fooo>();
    }
}
