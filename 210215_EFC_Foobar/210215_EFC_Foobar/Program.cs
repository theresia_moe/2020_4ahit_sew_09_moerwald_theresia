﻿using System;
using System.Linq;

namespace _210215_EFC_Foobar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            FooBarContext context = new FooBarContext();

            //Bar b = new Bar() { Name = "Bar" };
            //FirstBar b1 = new FirstBar() { Name = "FirstBar" };
            //SecondBar b2 = new SecondBar() { Name = "SecondBar" };
            //ThirdBar b3 = new ThirdBar() { Name = "ThirdBar" };

            //context.Bars.Add(b);
            //context.Bars.Add(b1);
            //context.Bars.Add(b2);
            //context.Bars.Add(b3);
            //context.SaveChanges();
            //foreach (var item in context.Bars.ToList())
            //{
            //    Console.WriteLine(item);
            //}
            //Foo f = new Foo() { Name = "Foo1" };
            //FirstFoo f1 = new FirstFoo() { Name = "FirstFoo", First = "First" };
            //SecondFoo f2 = new SecondFoo() { Name = "SecondFoo", Second = "Secound" };
            //ThirdFoo f3 = new ThirdFoo() { Name = "ThirdFoo", Third = "Third" };
            //context.Foos.Add(f);
            //context.FirstFoos.Add(f1);
            //context.SecondFoos.Add(f2);
            //context.ThirdFoos.Add(f3);
            //context.SaveChanges();
            //foreach (var item in context.Foos.ToList())
            //{
            //    Console.WriteLine(item);
            //}

            //Bar b1 = new Bar() { Name = "Bar1" };
            //Bar b2 = new Bar() { Name = "Bar2" };
            //Bar b3 = new Bar() { Name = "Bar3" };
            //context.Bars.Add(b1);
            //context.Bars.Add(b2);
            //context.Bars.Add(b3);
            //context.SaveChanges();
            //Foo f = new Foo() { Name = "Foo" };
            //context.Foos.Add(f);
            //context.SaveChanges();
            //Foobar fb = new Foobar() { OneFoo = f };
            //fb.ListBar.Add(b1);
            //fb.ListBar.Add(b2);
            //fb.ListBar.Add(b3);
            //context.Foobars.Add(fb);
            //context.SaveChanges();

            //Foobar loadFb = context.Foobars.ToList().First();
            //Console.WriteLine(loadFb.OneFoo.Name);
            //foreach( var bar in loadFb.ListBar)
            //{
            //    Console.WriteLine(bar.Name);
            //}

            //Fooo fooo = new Fooo();
            //Baar baar = new Baar();
            //int count = 10;
            //for (int i = 0; i < count; i++)
            //{
            //    fooo.Baars.Add(new Baar() { Name = $"Fooo{i}" });
            //    baar.Fooos.Add(new Fooo() { Name = $"Baar{i}" });
            //}

            //context.Fooos.Add(fooo);
            //context.Baars.Add(baar);
            //context.SaveChanges();

            //var f0 = new Foo() { Name = "Bill" };
            //var f1 = new Foo() { Name = "Max" };
            //context.Foos.Add(f0);
            //context.Foos.Add(f1);
            //var b0 = new Bar() { Name = "Fritz" };
            //context.Bars.Add(b0);
            //context.SaveChanges();
            Action<string> print = Console.WriteLine;
            //var foos = context.Foos.ToList();
            //foos.ForEach(x => print(x.ToString()));
            //var b = context.Foos.Where(s => s.Name.StartsWith("B")).ToList();
            //b.ForEach(x => print(x.ToString()));
            //Console.WriteLine(context.Bars.First());


            var b0 = new Bar() { Name = "Fritz" };
            var b1 = new Bar() { Name = "Kurt" };
            context.Bars.Add(b0);
            context.Bars.Add(b1);
            context.SaveChanges();
            Bar firstBar = context.Bars.First();
            var bars = context.Bars.ToList();
            bars.ForEach(x => print(x.ToString()));
            firstBar.Name = "NewName";
            context.SaveChanges();
            bars.ForEach(x => print(x.ToString()));





        }

        }
}
