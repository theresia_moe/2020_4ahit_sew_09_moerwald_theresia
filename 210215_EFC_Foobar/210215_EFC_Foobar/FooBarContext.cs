﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace _210215_EFC_Foobar
{
    public class FooBarContext:DbContext
    {

        public static readonly ILoggerFactory loggerFactory =
            LoggerFactory.Create(builder => { builder.AddConsole(); });

        //Constructor
        public FooBarContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //Set Connection String
            optionsBuilder
                .UseLoggerFactory(loggerFactory)
                .EnableSensitiveDataLogging()
                .UseSqlServer(
                @"Server=(localdb)\MSSQLLocalDB;
                Database=basic1;Trusted_Connection=True;");

            Console.WriteLine("Connection SET");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Foo>().Property<DateTime>("CreatedDate");
            modelBuilder.Entity<Foo>().Property<DateTime>("UpdatedDate");
        }

        //Add Entities to the DataBase
        public DbSet<Foo> Foos { get; set; }
        //public DbSet<FirstFoo> FirstFoos { get; set; }
        //public DbSet<SecondFoo> SecondFoos { get; set; }
        //public DbSet<ThirdFoo> ThirdFoos { get; set; }

        public DbSet<Bar> Bars { get; set; }
        public DbSet<Foobar> Foobars { get; set; }
        //public DbSet<FirstBar> FirstBars { get; set; }
        //public DbSet<SecondBar> SecondBars { get; set; }
        //public DbSet<ThirdBar> ThirdBars { get; set; }

        public DbSet<Fooo>Fooos { get; set; }
        public DbSet<Baar> Baars { get; set; }

    }
}
