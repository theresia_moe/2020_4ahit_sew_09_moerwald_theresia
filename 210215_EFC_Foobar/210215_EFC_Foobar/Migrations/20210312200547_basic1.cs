﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace _210215_EFC_Foobar.Migrations
{
    public partial class basic1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "Timestamp",
                table: "Bars",
                type: "rowversion",
                rowVersion: true,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "Bars");
        }
    }
}
