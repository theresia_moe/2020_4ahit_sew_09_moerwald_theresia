﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace _210215_EFC_Foobar.Migrations
{
    public partial class relations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FirstFoo");

            migrationBuilder.DropTable(
                name: "SecondFoo");

            migrationBuilder.DropTable(
                name: "ThirdFoo");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Bars");

            migrationBuilder.RenameColumn(
                name: "FooId",
                table: "Foos",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "BarId",
                table: "Bars",
                newName: "Id");

            migrationBuilder.AddColumn<int>(
                name: "FoobarId",
                table: "Bars",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Baars",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Baars", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Foobars",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OneFooId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Foobars", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Foobars_Foos_OneFooId",
                        column: x => x.OneFooId,
                        principalTable: "Foos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Fooos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fooos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BaarFooo",
                columns: table => new
                {
                    BaarsId = table.Column<int>(type: "int", nullable: false),
                    FooosId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaarFooo", x => new { x.BaarsId, x.FooosId });
                    table.ForeignKey(
                        name: "FK_BaarFooo_Baars_BaarsId",
                        column: x => x.BaarsId,
                        principalTable: "Baars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BaarFooo_Fooos_FooosId",
                        column: x => x.FooosId,
                        principalTable: "Fooos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Bars_FoobarId",
                table: "Bars",
                column: "FoobarId");

            migrationBuilder.CreateIndex(
                name: "IX_BaarFooo_FooosId",
                table: "BaarFooo",
                column: "FooosId");

            migrationBuilder.CreateIndex(
                name: "IX_Foobars_OneFooId",
                table: "Foobars",
                column: "OneFooId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bars_Foobars_FoobarId",
                table: "Bars",
                column: "FoobarId",
                principalTable: "Foobars",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bars_Foobars_FoobarId",
                table: "Bars");

            migrationBuilder.DropTable(
                name: "BaarFooo");

            migrationBuilder.DropTable(
                name: "Foobars");

            migrationBuilder.DropTable(
                name: "Baars");

            migrationBuilder.DropTable(
                name: "Fooos");

            migrationBuilder.DropIndex(
                name: "IX_Bars_FoobarId",
                table: "Bars");

            migrationBuilder.DropColumn(
                name: "FoobarId",
                table: "Bars");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Foos",
                newName: "FooId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Bars",
                newName: "BarId");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Bars",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "FirstFoo",
                columns: table => new
                {
                    FooId = table.Column<int>(type: "int", nullable: false),
                    First = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FirstFoo", x => x.FooId);
                    table.ForeignKey(
                        name: "FK_FirstFoo_Foos_FooId",
                        column: x => x.FooId,
                        principalTable: "Foos",
                        principalColumn: "FooId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SecondFoo",
                columns: table => new
                {
                    FooId = table.Column<int>(type: "int", nullable: false),
                    Second = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SecondFoo", x => x.FooId);
                    table.ForeignKey(
                        name: "FK_SecondFoo_Foos_FooId",
                        column: x => x.FooId,
                        principalTable: "Foos",
                        principalColumn: "FooId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ThirdFoo",
                columns: table => new
                {
                    FooId = table.Column<int>(type: "int", nullable: false),
                    Third = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThirdFoo", x => x.FooId);
                    table.ForeignKey(
                        name: "FK_ThirdFoo_Foos_FooId",
                        column: x => x.FooId,
                        principalTable: "Foos",
                        principalColumn: "FooId",
                        onDelete: ReferentialAction.Restrict);
                });
        }
    }
}
