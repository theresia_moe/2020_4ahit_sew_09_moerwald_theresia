﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace _210215_EFC_Foobar.Migrations
{
    public partial class inheritanceTPT : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Foos");

            migrationBuilder.DropColumn(
                name: "First",
                table: "Foos");

            migrationBuilder.DropColumn(
                name: "Second",
                table: "Foos");

            migrationBuilder.DropColumn(
                name: "Third",
                table: "Foos");

            migrationBuilder.CreateTable(
                name: "FirstFoo",
                columns: table => new
                {
                    FooId = table.Column<int>(type: "int", nullable: false),
                    First = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FirstFoo", x => x.FooId);
                    table.ForeignKey(
                        name: "FK_FirstFoo_Foos_FooId",
                        column: x => x.FooId,
                        principalTable: "Foos",
                        principalColumn: "FooId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SecondFoo",
                columns: table => new
                {
                    FooId = table.Column<int>(type: "int", nullable: false),
                    Second = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SecondFoo", x => x.FooId);
                    table.ForeignKey(
                        name: "FK_SecondFoo_Foos_FooId",
                        column: x => x.FooId,
                        principalTable: "Foos",
                        principalColumn: "FooId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ThirdFoo",
                columns: table => new
                {
                    FooId = table.Column<int>(type: "int", nullable: false),
                    Third = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThirdFoo", x => x.FooId);
                    table.ForeignKey(
                        name: "FK_ThirdFoo_Foos_FooId",
                        column: x => x.FooId,
                        principalTable: "Foos",
                        principalColumn: "FooId",
                        onDelete: ReferentialAction.Restrict);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FirstFoo");

            migrationBuilder.DropTable(
                name: "SecondFoo");

            migrationBuilder.DropTable(
                name: "ThirdFoo");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Foos",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "First",
                table: "Foos",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Second",
                table: "Foos",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Third",
                table: "Foos",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
