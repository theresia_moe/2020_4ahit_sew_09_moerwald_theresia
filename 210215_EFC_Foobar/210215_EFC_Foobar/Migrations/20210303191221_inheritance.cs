﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace _210215_EFC_Foobar.Migrations
{
    public partial class inheritance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Foos",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "First",
                table: "Foos",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Second",
                table: "Foos",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Third",
                table: "Foos",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Bars",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Foos");

            migrationBuilder.DropColumn(
                name: "First",
                table: "Foos");

            migrationBuilder.DropColumn(
                name: "Second",
                table: "Foos");

            migrationBuilder.DropColumn(
                name: "Third",
                table: "Foos");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Bars");
        }
    }
}
