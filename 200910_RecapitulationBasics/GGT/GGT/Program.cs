﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGT
{
    class Program
    {
        static void Main(string[] args)
        {
            int zahl1, zahl2;            
            zahl1 = 64;
            zahl2 = 54;

            int ggt = FunctionGGT(zahl1, zahl2);
            Console.WriteLine(ggt);
            Console.ReadLine();
        }


        static int FunctionGGT(int zahl1, int zahl2)
        {
            if (zahl2 == 0)
                return zahl1;
            else
            {
                return FunctionGGT(zahl2, zahl1 % zahl2);
            }
        }
    }
}

