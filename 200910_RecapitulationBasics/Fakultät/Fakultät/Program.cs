﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakultät
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 6;                  // Zahl von der die Fakultät berechnet werden soll
            int result = Faculty(i);    // Fakultät von i
            Console.WriteLine(result);  // Ausgabe
            Console.ReadLine();
        }

        public static int Faculty(int i)
        {

            if (i == 0)                 // wenn i = 0, dann ist die Fakultät 0
                return 1;
            else
                return i * Faculty(i - 1); // rekursiver Methoden aufruf
        }
    }
}
