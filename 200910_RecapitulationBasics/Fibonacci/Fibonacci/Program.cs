﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            Fibonacci(1, 0);

            Console.ReadLine();
            
        }
        static void Fibonacci(int i, int j)
        {
            i = i + j;
            j = i - j;

            Console.WriteLine(i);

            if (i < 100000)
                Fibonacci(i, j);
            else
                Console.WriteLine("Ende");

        }
    }
}
