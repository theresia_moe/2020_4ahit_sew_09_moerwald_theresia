﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Begriffserkärung_new_override
{
    class Program
    {
        static void Main(string[] args)
        {
            //Begriffserklärung


            //überdecken vs. überschreiben (new vs. override):
            // Der Unterschied zwischen Override und New besteht darin,
            //dass Override die Methode der Basisklasse mit neuer Definition erweitert,
            //New aber die Methode der Basisklasse ausblendet. 

            // Beispiel zu override and new


            Animal cat = new Animal();
            cat.Eat(); // output: The animal is eating.

            Fish memo = new Fish();
            memo.Eat(); // output: The fish is eating.

            Horse blacky = new Horse();
            blacky.Eat(); //output: The horse is eating.

            Animal fish = new Fish(); //Fish is the class we used override
            fish.Eat(); //so the output is: The fish is eating

            Animal horse = new Horse(); //Horse is the class we used new. (überdecken)
            horse.Eat(); //so the output is: The animal is eating

            Console.ReadLine();
        }

        class Animal{
            public virtual void Eat()
            {
                Console.WriteLine("The animal is eating.");
            }
        }
        class Fish : Animal{
            public override void Eat()
            {
                Console.WriteLine("The fish is eating.");

            }
        }
        class Horse : Animal { }
            public new void Eat()
            {
                Console.WriteLine("The horse is eating.");

            }
        }
    }
    

