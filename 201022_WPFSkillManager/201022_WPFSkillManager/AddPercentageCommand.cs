﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _201022_WPFSkillManager
{
    class AddPercentageCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        SkillsVM parent;
        public AddPercentageCommand(SkillsVM vm)
        {
            parent = vm;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            parent.AddPercentage();
        }
    }
}
