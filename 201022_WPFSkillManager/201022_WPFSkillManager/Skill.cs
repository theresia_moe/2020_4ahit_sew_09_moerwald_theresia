﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201022_WPFSkillManager
{
    class Skill
    {

        public string Name { get; set; }
        int percentage;
        public Skill() { }

        public Skill(string name, int percentage)
        {
            Name = name;
            Percentage = percentage;
        }

        public int Percentage
        {
            get
            {
                return percentage;
            }
            set
            {

                percentage = value;


            }
        }



    }
}

