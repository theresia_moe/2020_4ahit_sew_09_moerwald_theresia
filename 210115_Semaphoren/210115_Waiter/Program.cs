﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _210115_Waiter
{
    class Program
    {
        private static SemaphoreSlim semPreparation;
        private static SemaphoreSlim semMixer;
        private static int waiters = 10;
        private static int preparePlaces = 3;
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.Clear();
            semPreparation = new SemaphoreSlim(preparePlaces);
            semMixer = new SemaphoreSlim(1);
            StartCompetition();

        }

        public static void StartCompetition()
        {
            Thread[] threads = new Thread[waiters];
            for (int i = 0; i < waiters; i++)
            {
                threads[i] = new Thread(PrepareCocktail);
                threads[i].Name = "Kellner " + (i + 1);
            }
            Parallel.ForEach(threads, t => t.Start());
            Console.WriteLine("Besprechung");
            Thread.Sleep(500);

        }

        private static void PrepareCocktail()
        {
            semPreparation.Wait();
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("+++ {0} betritt die Bar", Thread.CurrentThread.Name);
            MixCocktail();
            Thread.Sleep(500);
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine("--- {0} verlässt die Bar", Thread.CurrentThread.Name);
            Console.ForegroundColor = ConsoleColor.Black;
            semPreparation.Release();
        }

        private static void MixCocktail()
        {
            semMixer.Wait();
            Console.WriteLine("\t{0} nutzt den Mixer", Thread.CurrentThread.Name);
            Thread.Sleep(500);
            Console.WriteLine("\t{0} gibt den Mixer frei", Thread.CurrentThread.Name);
            semMixer.Release();

        }

    }
}

