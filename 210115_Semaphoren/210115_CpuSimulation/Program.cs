﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _210115_CpuSimulation
{
    class Program
    {
        static void Main(string[] args)
        {
            List<MyThread> threads = new List<MyThread>
            {
                new MyThread("Thread 1"),
                new MyThread("Thread 2"),
                new MyThread("Thread 3")
            };

            threads.ForEach(thread => {
                try
                {
                    ReadyQueue.GetInstance().Add(thread);
                }
                catch (ThreadInterruptedException ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            });

            Thread readyQueueThread = new Thread(new ThreadStart(ReadyQueueConsumer.Run));
            readyQueueThread.Start();

            Thread runningQueueThread = new Thread(new ThreadStart(RunningQueueConsumer.Run));
            runningQueueThread.Start();

            Thread blockedQueueThread = new Thread(new ThreadStart(BlockedQueueConsumer.Run));
            blockedQueueThread.Start();
        }

    }
}
