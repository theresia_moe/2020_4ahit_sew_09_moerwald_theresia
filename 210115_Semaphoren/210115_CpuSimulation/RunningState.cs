﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _210115_CpuSimulation
{
    class RunningState : IState
    {
        private Random randi = new Random();
        public IState ChangeState(MyThread thread)
        {
            Thread.Sleep(500);

            int pivot = randi.Next(0, 101);
            Console.WriteLine("Pivot: " + pivot);
            if (pivot <= 10)
            {
                return new FinishedState();
            }
            if (pivot > 10 && pivot <= 40)
            {
                BlockedQueue.GetInstance().Add(thread);
                return new BlockedState();
            }
            ReadyQueue.GetInstance().Add(thread);
            return new ReadyState();
        }
        public override string ToString()
        {
            return "Running State";
        }
    }
}
