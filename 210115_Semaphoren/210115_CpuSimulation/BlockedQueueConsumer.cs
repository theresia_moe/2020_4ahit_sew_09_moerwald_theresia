﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _210115_CpuSimulation
{
    class BlockedQueueConsumer
    {
		public static void Run()
		{
			while (true)
			{
				try
				{
					MyThread thread = BlockedQueue.GetInstance().Take();
					thread.NextState();
				}
				catch (ThreadInterruptedException ex)
				{
					Console.WriteLine(ex.StackTrace);
				}
			}
		}
	}
}
