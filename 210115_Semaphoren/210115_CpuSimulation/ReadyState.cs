﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _210115_CpuSimulation
{
    class ReadyState : IState
    {
        public IState ChangeState(MyThread thread)
        {
            try
            {
                ReadyQueue.GetInstance().Add(thread);
            }
            catch (ThreadInterruptedException ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return new RunningState();
        }
        public override string ToString()
        {
            return "Ready State";
        }
    }
}
