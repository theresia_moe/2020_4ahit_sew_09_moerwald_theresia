﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210115_CpuSimulation
{
    interface IState
    {
        IState ChangeState(MyThread thread);
    }
}
