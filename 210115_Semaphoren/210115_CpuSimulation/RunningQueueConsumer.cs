﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _210115_CpuSimulation
{
    class RunningQueueConsumer
    {
		public static void Run()
		{
			while (true)
			{
				try
				{
					MyThread thread = RunningQueue.GetInstance().Take();
					thread.NextState();
				}
				catch (ThreadInterruptedException ex)
				{
					Console.WriteLine(ex.StackTrace);
				}
			}
		}
	}
}
