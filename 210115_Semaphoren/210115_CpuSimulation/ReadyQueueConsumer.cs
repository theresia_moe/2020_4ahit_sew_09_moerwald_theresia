﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _210115_CpuSimulation
{
    class ReadyQueueConsumer
    {
		public static void Run()
		{
			while (true)
			{
				try
				{
					MyThread thread = ReadyQueue.GetInstance().Take();
					thread.NextState();
				}
				catch (ThreadInterruptedException ex)
				{
					Console.WriteLine(ex.StackTrace);
				}
			}
		}
	}
}
