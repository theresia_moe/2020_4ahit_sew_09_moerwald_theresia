﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _210115_CpuSimulation
{
    class BlockedState : IState
    {
        public IState ChangeState(MyThread thread)
        {
            try
            {
                BlockedQueue.GetInstance().Add(thread);
            }
            catch (ThreadInterruptedException ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return new ReadyState();
        }
        public override string ToString()
        {
            return "Blocked State";
        }
    }
}
