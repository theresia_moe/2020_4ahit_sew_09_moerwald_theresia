﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;

namespace _210115_CpuSimulation
{
    class RunningQueue : BlockingCollection<MyThread>
    {
        private static RunningQueue threads = new RunningQueue();

        public static RunningQueue GetInstance() { return threads; }

        private RunningQueue() : base(100)
        {
        }
    }
}
