﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210115_CpuSimulation
{
    class ReadyQueue : BlockingCollection<MyThread>
    {

        private static ReadyQueue threads = new ReadyQueue();

        public static ReadyQueue GetInstance() { return threads; }

        private ReadyQueue() : base(100) //Begrenzte Größe der Auflistung
        {
        }
    }
}
