﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _210115_CpuSimulation
{
    class FinishedState : IState
    {
        public IState ChangeState(MyThread thread)
        {
            return new FinishedState();
        }
        public override string ToString()
        {
            return "Finished State";
        }
    }
}
