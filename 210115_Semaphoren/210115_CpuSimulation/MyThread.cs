﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace _210115_CpuSimulation
{
    class MyThread : ISerializable
    {
        private IState state = new ReadyState();

        private string name;

        public void NextState()
        {
            state = state.ChangeState(this);
            Console.WriteLine(state.ToString() + ": " + name);
        }

        public MyThread(string name = "")
        {
            this.name = name;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }
    }
}
