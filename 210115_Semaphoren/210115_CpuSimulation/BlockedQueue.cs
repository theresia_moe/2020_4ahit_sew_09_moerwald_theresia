﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210115_CpuSimulation
{
    class BlockedQueue: BlockingCollection<MyThread>
    {
        private static BlockedQueue threads = new BlockedQueue();

        public static BlockedQueue GetInstance() { return threads; }

        private BlockedQueue() : base(100)
        {
        }
    }
}
