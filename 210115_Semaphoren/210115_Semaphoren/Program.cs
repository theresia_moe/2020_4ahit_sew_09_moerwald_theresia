﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _210115_Semaphore
{
    class Program
    {
        static Semaphore threadPool = new Semaphore(3, 5);
        private static void PerformSomeWork()
        {
            threadPool.WaitOne();
            Console.WriteLine("Thread {0} is inside the critical section...", Thread.CurrentThread.Name);
            Thread.Sleep(10000);
            threadPool.Release();
        }
        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                Thread threadObject = new Thread(new ThreadStart(PerformSomeWork));
                threadObject.Name = "Thread Name: " + i;
                threadObject.Start();
            }
        }
    }
}
