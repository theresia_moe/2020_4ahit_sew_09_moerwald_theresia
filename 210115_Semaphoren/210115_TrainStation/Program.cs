﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _210115_TrainStation
{
    class Program
    {
        private static readonly SemaphoreSlim AtoBSlim = new SemaphoreSlim(3);
        private static readonly SemaphoreSlim BtoCSlim = new SemaphoreSlim(4);
        private static readonly SemaphoreSlim BStationSlim = new SemaphoreSlim(5);
        static readonly int trains = 30;
        static void Main(string[] args)
        {
            for (int i = 0; i < trains; i++)
            {
                Thread t = new Thread(Journey);
                t.Name = "Train 1-" + (i + 1);
                t.Start();

                Thread t2 = new Thread(JourneyHome);
                t2.Name = "Train 2-" + (i + 1);
                t2.Start();
            }
        }

        private static void Journey()
        {
            TrackFromAToB();
            Station();
            TrackFromBToC();
        }
        private static void JourneyHome()
        {
            TrackFromCToB();
            Station();
            TrackFromBToA();
        }

        public static void Station()
        {
            BStationSlim.Wait();
            Console.WriteLine("{0} is currently in station B", Thread.CurrentThread.Name);
            BStationSlim.Release();
        }
        public static void TrackFromAToB()
        {
            AtoBSlim.Wait();
            Console.WriteLine("{0} is going from A to B", Thread.CurrentThread.Name);
            AtoBSlim.Release();
        }
        public static void TrackFromBToC()
        {
            BtoCSlim.Wait();
            Console.WriteLine("{0} is going from B to C", Thread.CurrentThread.Name);
            BtoCSlim.Release();
        }
        public static void TrackFromBToA()
        {
            AtoBSlim.Wait();
            Console.WriteLine("{0} is going from B to A", Thread.CurrentThread.Name);
            AtoBSlim.Release();
        }
        public static void TrackFromCToB()
        {
            BtoCSlim.Wait();
            Console.WriteLine("{0} is going from C to B", Thread.CurrentThread.Name);
            BtoCSlim.Release();
        }
    }
}
