using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _210521_AspNet_InOut_Expenses.Models;
using Microsoft.AspNetCore.Mvc;

namespace _210521_AspNet_InOut_Expenses.Controllers
{
    public class ItemController : Controller {
        private readonly InOutDbContext _db;

        public ItemController(InOutDbContext db) {
            _db = db;
        }

        public IActionResult Index() {
            IEnumerable<Item> objList = _db.Items;
            return View(objList);
        }
     
        // GET-Create
        public IActionResult Create()
        {
            return View();
        }

        // POST-Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Item obj)
        {
            _db.Items.Add(obj);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }
        
    }
}