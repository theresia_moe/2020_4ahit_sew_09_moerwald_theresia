﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace _210521_AspNet_InOut_Expenses.Models {
    public class ExpenseVM {
        public Expense Expense { get; set; }
        public IEnumerable<SelectListItem> TypeDropDown { get; set; }
    }
}