﻿using System.ComponentModel.DataAnnotations;

namespace _210521_AspNet_InOut_Expenses.Models {
    public class ExpenseType {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}