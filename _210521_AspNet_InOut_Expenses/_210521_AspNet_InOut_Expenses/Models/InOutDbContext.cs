﻿

using Microsoft.EntityFrameworkCore;

namespace _210521_AspNet_InOut_Expenses.Models {
    public class InOutDbContext : DbContext{
        public DbSet<Item> Items { get; set; }
        
        public DbSet<Expense> Expenses { get; set; }
        public DbSet<ExpenseType> ExpenseTypes { get; set; }

        public InOutDbContext() {
            
        }

        public InOutDbContext(DbContextOptions<InOutDbContext> options) : base(options) {
            
        }
    }
}