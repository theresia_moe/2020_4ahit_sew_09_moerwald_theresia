﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace _210521_AspNet_InOut_Expenses.Models {
    
    
    public class Item {
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }
        
        [Required]
        [StringLength(100)]
        public string Borrower { get; set; }

        [Required]
        [StringLength(100)]
        public string Lender { get; set; }
        
    }
}