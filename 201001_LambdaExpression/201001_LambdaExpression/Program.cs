﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201001_LambdaExpression
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Employee> employees = new List<Employee>()
            {
                new Employee("Fritz", 45),
                new Employee("Brigitte", 36),
                new Employee("Hans", 41),
                new Employee("Jürgen", 27)
            };

            List<Employee> foundEmployees = new List<Employee>();

            foundEmployees = employees.FindAll(e1 =>
            {
                return (e1.Age > 35);
            });

            foreach (Employee employee in foundEmployees)
            {
                Console.WriteLine($"Name: {employee.Name} Age: {employee.Age}");
            }
    
            foundEmployees = MyFindAll<Employee>(employees, (e1 =>
            {
                return (e1.Age > 35);
            }));
            Console.WriteLine("Zusatz 1:");
            foreach (Employee employee in foundEmployees)
            {
                Console.WriteLine($"Name: {employee.Name} Age: {employee.Age}");
            }

            Console.ReadLine();
 
        }
     
        static List<T> MyFindAll<T>(List<T> list, Predicate<T> condition)
        {
            List<T> foundlist = new List<T>();
            foreach (T element in list)
            {
                if (condition(element))
                    foundlist.Add(element);
            }
            return foundlist;
        }
    }

}
