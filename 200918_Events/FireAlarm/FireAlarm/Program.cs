﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FireAlarm
{
    class Program
    {
        static void Main(string[] args)
        {
            TestFireAlarm();

            Console.ReadLine();
        }

        static void TestFireAlarm()
        {
            School school = new School();
            Clazz clazz = new Clazz("4AHIT");
            Pupil p1 = new Pupil("Markus Kluka", school, clazz);
            Pupil p2 = new Pupil("Julian Strondl", school, clazz);
            clazz = new Clazz("4BHIT");
            Pupil p3 = new Pupil("Jasmin Gratz", school, clazz);
            school.StartFireAlarm("Probealarm");
        }
    }
}
