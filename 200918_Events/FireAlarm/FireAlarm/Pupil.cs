﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FireAlarm
{
    class Pupil
    {
        private string name;
        private Clazz clazz;
        private School school;
        public Pupil(string name, School school, Clazz clazz)
        {
            this.name = name;
            this.school = school;
            this.clazz = clazz;
            school.FireAlarm += EscapeRoute;
        }
        private void EscapeRoute(string type)
        {
            Console.WriteLine($"{type}! Schüler: {name} begibt sich in den Hof {school.GetPlaceNumber(clazz)}");
        }
    }
}
