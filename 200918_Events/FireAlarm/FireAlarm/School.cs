﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FireAlarm
{
    class School
    {
       
        public delegate void FireDelegate(string type);
        public event FireDelegate FireAlarm;
        public School() { }
        public int GetPlaceNumber(Clazz c)
        {
            switch (c.Name)
            {
                case "4AHIT":
                    return 1;
                case "4BHIT":
                    return 2;
            }
            return 0;
        }
        public void StartFireAlarm(string type)
        {
            FireAlarm(type);
        }

    }
}
