﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HappyBirthdayEvents
{
    public delegate string BirthdayDelegate(string s);
    public class Person
    {
         public event BirthdayDelegate SendWishes;

        public Person()
        {
            SendWishes += new BirthdayDelegate(this.Wishes);

        }

        public string Wishes(string nickname)
        {
            return "Happy Birthday " + nickname;
        }

        public static void Test()
        {
            Person p = new Person();
            string greetings = p.SendWishes("Marry");
            Console.WriteLine(greetings);
        }
    }
}
