﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace _20210224_EFC_University.Migrations
{
    public partial class relations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PersonEventsId",
                table: "Persons",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PersonsId",
                table: "Events",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PersonEvents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LocationId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonEvents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PersonEvents_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StudentStudyPaths",
                columns: table => new
                {
                    StudentsId = table.Column<int>(type: "int", nullable: false),
                    StudyPathsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentStudyPaths", x => new { x.StudentsId, x.StudyPathsId });
                    table.ForeignKey(
                        name: "FK_StudentStudyPaths_Student_StudentsId",
                        column: x => x.StudentsId,
                        principalTable: "Student",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentStudyPaths_StudyPaths_StudyPathsId",
                        column: x => x.StudyPathsId,
                        principalTable: "StudyPaths",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Persons_PersonEventsId",
                table: "Persons",
                column: "PersonEventsId");

            migrationBuilder.CreateIndex(
                name: "IX_Events_PersonsId",
                table: "Events",
                column: "PersonsId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonEvents_LocationId",
                table: "PersonEvents",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentStudyPaths_StudyPathsId",
                table: "StudentStudyPaths",
                column: "StudyPathsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Events_Persons_PersonsId",
                table: "Events",
                column: "PersonsId",
                principalTable: "Persons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Persons_PersonEvents_PersonEventsId",
                table: "Persons",
                column: "PersonEventsId",
                principalTable: "PersonEvents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_Persons_PersonsId",
                table: "Events");

            migrationBuilder.DropForeignKey(
                name: "FK_Persons_PersonEvents_PersonEventsId",
                table: "Persons");

            migrationBuilder.DropTable(
                name: "PersonEvents");

            migrationBuilder.DropTable(
                name: "StudentStudyPaths");

            migrationBuilder.DropIndex(
                name: "IX_Persons_PersonEventsId",
                table: "Persons");

            migrationBuilder.DropIndex(
                name: "IX_Events_PersonsId",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "PersonEventsId",
                table: "Persons");

            migrationBuilder.DropColumn(
                name: "PersonsId",
                table: "Events");
        }
    }
}
