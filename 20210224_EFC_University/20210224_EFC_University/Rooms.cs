﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace _20210224_EFC_University
{
    class Rooms
    {
        public int Id { get; set; }

        public char Wing { get; set; }

        public ERoomType RoomType { get; set; }
        [Timestamp]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Wing.ToString();
        }

        public enum ERoomType
        {
            EDVRoom,
            LectureRoom,
            Laboratory
        }
    }
}
