﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace _20210224_EFC_University
{
    class Events
    {
        public int Id { get; set; }
        public int Duration { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

      
        public override string ToString()
        {
            return Duration.ToString();
        }
    }

    class Exam : Events { }
    class Lecture : Events { }
    class ProjectWork : Events { }

}
