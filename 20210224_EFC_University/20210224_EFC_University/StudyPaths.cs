﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace _20210224_EFC_University
{
    class StudyPaths
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public List<Student> Students { get; set; } = new List<Student>(); //Many to Many

        [Timestamp]
        public byte[] Timestamp { get; set; }


        public override string ToString()
        {
            return Description;
        }
    }
}
