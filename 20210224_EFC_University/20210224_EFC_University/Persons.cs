﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace _20210224_EFC_University
{
    class Persons
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Events> Events { get; set; }  //Many to Many Relation

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public override string ToString()
        {
            return Name;
        }
     
    }

    [Table("Student")]
    class Student:Persons{
        public int MatriculationNumber { get; set; }
        public ESubject Subject { get; set; }

        public List<StudyPaths> StudyPaths { get; set; } = new List<StudyPaths>(); //Many to Many



    }
    [Table("Professor")]
    class Professor : Persons
    {
        public string Title { get; set; }

    }
    [Table("Assistent")]
    class Assistent : Persons
    {
        public double Salary { get; set; }

    }

    public enum ESubject
    {
        Educationalscience,
        Politicalscience,
        Psychology,
        Sociology,
        Sports,
        Theology,
        BigData
    }
}
