﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace _20210224_EFC_University
{
    class UniversityContext:DbContext
    {
        public static readonly ILoggerFactory loggerFactory = LoggerFactory.Create(builder => { builder.AddConsole(); });
        public UniversityContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
            .UseLoggerFactory(loggerFactory)
            .EnableSensitiveDataLogging()
            .UseSqlServer(
            @"Server=(localdb)\MSSQLLocalDB;
            Database= special;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Events>().Property<DateTime>("CreatedDate");
            modelBuilder.Entity<Events>().Property<DateTime>("UpdatedDate");
            modelBuilder.Entity<Persons>().Property<DateTime>("CreatedDate");
            modelBuilder.Entity<Persons>().Property<DateTime>("UpdatedDate");
            modelBuilder.Entity<Rooms>().Property<DateTime>("CreatedDate");
            modelBuilder.Entity<Rooms>().Property<DateTime>("UpdatedDate");
            modelBuilder.Entity<StudyPaths>().Property<DateTime>("CreatedDate");
            modelBuilder.Entity<StudyPaths>().Property<DateTime>("UpdatedDate");
            modelBuilder.Entity<Location>().Property<DateTime>("CreatedDate");
            modelBuilder.Entity<Location>().Property<DateTime>("UpdatedDate");
        }

        public DbSet<Events> Events { get; set; }

        public DbSet<Lecture> Lectures { get; set; }
        public DbSet<Exam> Exams { get; set; }
        public DbSet<ProjectWork> ProjectWorks { get; set; }

        public DbSet<Location> Locations { get; set; }
        public DbSet<Persons> Persons { get; set; }

        public DbSet<Student> Students { get; set; }
        public DbSet<Professor> Professor { get; set; }
        public DbSet<Assistent> Assistents { get; set; }

        public DbSet<Rooms> Rooms { get; set; }
        public DbSet<StudyPaths> StudyPaths { get; set; }
        public DbSet<PersonEvents> PersonEvents { get; set; }
    }
}
