﻿using System;
using System.Linq;

namespace _20210224_EFC_University
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome To Entity Framework Core!");


            using (var context = new UniversityContext()) {

                var p1 = new Persons() { Name = "Theresia" };
                context.Persons.Add(p1);
                var r1 = new Rooms() { Wing ='A' };
                context.Rooms.Add(r1);
                var l1 = new Location() { PostalCode = 3493 };
                context.Locations.Add(l1);
                var s1 = new StudyPaths() { Description = "IT" };
                context.StudyPaths.Add(s1);
                var e1 = new Events() { Duration = 1 };
                context.Events.Add(e1);

                var student1 = new Student() { Name = "Theresia", MatriculationNumber = 1, Subject = ESubject.BigData };
                context.Students.Add(student1);

                var prof1 = new Professor() { Name = "Markus", Title = "Dr." };
                context.Professor.Add(prof1);

                var assisten1 = new Assistent() { Name = "Simon", Salary = 1800 };
                context.Assistents.Add(assisten1);

                var exam1 = new Exam() { Duration = 1 };
                context.Events.Add(exam1);

                var lecture1 = new Lecture() { Duration = 4 };
                context.Events.Add(lecture1);

                var pwork1 = new ProjectWork() { Duration = 3 };
                context.Events.Add(pwork1);

                //Realtions
                var pe = new PersonEvents() { Location = l1};
                pe.Participant.Add(student1);
                pe.Participant.Add(prof1);
                pe.Participant.Add(assisten1);

                context.PersonEvents.Add(pe);

                Student s = new Student();
                StudyPaths sp = new StudyPaths();
                for(int i = 0; i<5; i++)
                {
                    s.StudyPaths.Add(new StudyPaths() { Description = $"StudyPath{i}" });
                    sp.Students.Add(new Student() { Name = $"Student{i}" });
                }
                context.Students.Add(s);
                context.StudyPaths.Add(sp);


                context.SaveChanges();
                Console.WriteLine(context.Persons.First());
                Console.WriteLine(context.Rooms.First());
                Console.WriteLine(context.Locations.First());
                Console.WriteLine(context.StudyPaths.First());
                Console.WriteLine(context.Events.First());

                Console.WriteLine(context.Students.First());
                Console.WriteLine(context.Professor.First());
                Console.WriteLine(context.Assistents.First());

                Console.WriteLine(context.Events.First());

                Console.ReadLine();

            }
        }
    }
}
