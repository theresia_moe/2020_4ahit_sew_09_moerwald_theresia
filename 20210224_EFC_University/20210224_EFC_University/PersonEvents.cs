﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _20210224_EFC_University
{
    class PersonEvents
    {
        public int Id { get; set; }

        public Location Location { get; set; }  //One to One

        public List<Persons> Participant { get; set; } = new List<Persons>();//One to Many
    }
}
