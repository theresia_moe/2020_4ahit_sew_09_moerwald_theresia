﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace _20210224_EFC_University
{
    class Location
    {
        public int Id { get; set; }
        public int PostalCode { get; set; }
        [Timestamp]
        public byte[] Timestamp { get; set; }



        public override string ToString()
        {
            return PostalCode.ToString();
        }
    }
}
