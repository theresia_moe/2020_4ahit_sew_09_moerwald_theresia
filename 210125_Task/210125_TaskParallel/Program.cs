﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _210125_Task
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("For");
            for(int i = 0; i<10; i++)
            {
                Console.WriteLine("Sequential iteration on index {0} running on thread {1}.", i, Thread.CurrentThread.ManagedThreadId);
                Thread.Sleep(400);
            }

            Console.WriteLine("ParallelFor");
            Parallel.For(0, 10, i =>
            {
                Console.WriteLine("Sequential iteration on index {0} running on thread {1}.", i , Thread.CurrentThread.ManagedThreadId);
                Thread.Sleep(400);
            });

            Console.ReadLine();
        }
    }
}
