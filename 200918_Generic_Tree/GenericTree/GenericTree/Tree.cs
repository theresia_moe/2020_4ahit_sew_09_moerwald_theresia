﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericTree
{
    class Tree<T> where T:IComparable
    {

        public Node<T> root;

        public Tree()
        {
            root = null;
        }
                     
        public void Insert(T value)
        {

            Node<T> node = new Node<T>(value);
            Insert(node, ref root);

        }

        private void Insert(Node<T> node, ref Node<T> newone)
        {

            if (newone == null)
                newone = node;
            else if (node.data.CompareTo(newone.data) == -1)
                Insert(node, ref newone.left);
            else
                Insert(node, ref newone.right);
        }

        public void PrintInOrder()
        {
            PrintInOrder(root);
        }

        private void PrintInOrder(Node<T> current)
        {
            if (current.left != null)
                PrintInOrder(current.left);

            Console.Write(current.data + " ");

            if (current.right != null)
                PrintInOrder(current.right);
        }

        public void PrintPreOrder()
        {
            PrintPreOrder(root);
        }

        private void PrintPreOrder(Node<T> current)
        {

            Console.Write(current.data + " ");

            if (current.left != null)
                PrintPreOrder(current.left);

            if (current.right != null)
                PrintPreOrder(current.right);
        }

        public void PrintPostOrder()
        {
            PrintPostOrder(root);
        }

        private void PrintPostOrder(Node<T> current)
        {

            if (current.left != null)
                PrintPostOrder(current.left);

            if (current.right != null)
                PrintPostOrder(current.right);

            Console.Write(current.data + " ");
        }

    }
}
