﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericTree
{
    class Program
    {
        static void Main(string[] args)
        {
            Tree<int> t = new Tree<int>();

            t.Insert(6);
            t.Insert(3);
            t.Insert(9);
            t.Insert(2);
            t.Insert(7);
            t.Insert(1);
            t.Insert(8);
            t.Insert(4);


            t.PrintInOrder();
            Console.WriteLine();
            t.PrintPreOrder();
            Console.WriteLine();
            t.PrintPostOrder();



            Console.ReadLine();
        }
    }
}
