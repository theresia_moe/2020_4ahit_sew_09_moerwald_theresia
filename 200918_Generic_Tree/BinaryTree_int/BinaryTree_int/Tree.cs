﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree_int
{
    class Tree
    {
        public Node root;

       

        public void Insert(int value)
        {

            Node node = new Node(value);
            Insert(node, ref root);
     
        }

        private void Insert (Node node, ref Node newone)
        {
 
            if (newone == null)
                newone = node;
            else if (node.data <= newone.data)
                Insert(node, ref newone.left);
            else
                Insert(node, ref newone.right);
        }

        public void PrintInOrder()
        {
            PrintInOrder(root);
        }

        private void PrintInOrder(Node current)
        {
            if (current.left != null)
                PrintInOrder(current.left);

            Console.Write(current.data + " ");

            if (current.right != null)
                PrintInOrder(current.right);
        }

        public void PrintPreOrder()
        {
            PrintPreOrder(root);
        }

        private void PrintPreOrder(Node current)
        {

            Console.Write(current.data + " ");

            if (current.left != null)
                PrintPreOrder(current.left);
        
            if (current.right != null)
                PrintPreOrder(current.right);
        }

        public void PrintPostOrder()
        {
            PrintPostOrder(root);
        }

        private void PrintPostOrder(Node current)
        {
                   
            if (current.left != null)
                PrintPostOrder(current.left);

            if (current.right != null)
                PrintPostOrder(current.right);

            Console.Write(current.data + " ");
        }

    }
}